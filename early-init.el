;;; early-init.el --- yuumacs's bootstraper -*- lexical-binding: t; -*-

;; Copyright (C) 2022--2023  Yuu Yin <yuu@anche.no>
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;; Author: Yuu Yin <yuu@anche.no>
;;	Henrik Lissner
;; Maintainer: Yuu Yin <yuu@anche.no>
;; Created: 2022-07-24
;; Version: 2022.10-main
;; Keywords: yymacs, yy, emacs, configuration
;; Homepage: https://codeberg.org/yymacs/yymacs
;; Package-Requires: ((emacs "29.0"))

;; This file is not part of GNU Emacs.

;;; Commentary:

;; Loading order:
;;   > EMACSDIR/early-init.el
;;     > EMACSDIR/src/yy/elisp/yy.el
;;       - EMACSDIR/src/yy/elisp/yy-lib.el
;;     > EMACSDIR/src/yy/elisp/yy-start.el
;;   > EMACSDIR/init.el
;;     > `yyuu-directory'/yyuu.el

;;  Description


;;; Code:

;; flx buggy.
(setq-default native-comp-jit-compilation-deny-list '("^flx.*$"))


;;;; Performance

;; PERF: Garbage collection is a big contributor to startup times. This fends it
;; off, but yy will reset it. Not resetting it later will cause
;; stuttering/freezes (fr tho).
(setq gc-cons-threshold most-positive-fixnum)


;;;; Bootstrap

;; Load yy.
(load (expand-file-name "yy/yy" user-emacs-directory)
      nil (not init-file-debug) nil 'must-suffix)

;; Start yy.
(require 'yy-start)

;; Misc
;; (add-to-list 'load-path (expand-file-name "org/lisp/" package-user-dir))


;;; early-init.el ends here
