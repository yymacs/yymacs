;;; yyuu.el --- yyuurs -*- lexical-binding: t; -*-

;; Copyright (C) 2022--2023  Yuu Yin <yuu@anche.no>
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;; Author: Yuu Yin <yuu@anche.no>
;; Maintainer: Yuu Yin <yuu@anche.no>
;; Homepage: https://codeberg.org/yymacs/yyuu

;; Package-Requires:


;;; Commentary:


;;; Code:


;;;; Definition:

(defgroup yyuu nil
  "Custom GNU Emacs configuration for yλ universe"
  :link '(url-link "https://codeberg.org/yymacs/yymacs")
  :group 'environment)

(defconst yyuu-mod-directory
  (expand-file-name "mod/" yyuu-directory)
  "Modules directory.")

(defconst yyuu-enty-directory
  (expand-file-name "enty/" yyuu-directory)
  "Entities directory.")

(defconst yuu-directory
  (expand-file-name "yuu/" yyuu-enty-directory))


;;;; Load:

(add-to-list 'load-path yyuu-mod-directory)
(add-to-list 'load-path yuu-directory)


;;;; Entity:

(require 'yuu)


;;; yyuu.el ends here
