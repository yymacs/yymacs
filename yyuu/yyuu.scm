;;; manifest.scm --- Yuu's Guix DevOps for yymacs' yyuu -*- lexical-binding: t; -*-

;; Copyright (C) 2022--2023  Yuu Yin <yuu@anche.no>
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;; Author: Yuu Yin <yuu@anche.no>
;; Maintainer: Yuu Yin <yuu@anche.no>
;; Homepage: https://codeberg.org/yymacs/yyuu

;; Author: Yuu Yin <yuu@anche.no>
;; Maintainer: Yuu Yin <yuu@anche.no>
;; Created: 2022-07-07
;; Modified: 2022-07-07
;; Version: 2022.10-main
;; Keywords:
;; Homepage: https://codeberg.org/yymacs/yyuu

;; This file is part of yyuu.


;;; Commentary:
;; This "manifest" file can be passed to 'guix package --manifest=FILE' to
;; reproduce the content of your profile. This is "symbolic": it only specifies
;; package names. To reproduce the exact same profile, you also need to capture
;; the channels being used, as returned by "guix describe". See the "Replicating
;; Guix" section in the manual.


;;; Code:


;;;; Load

;; (let ((yyuu-path-root (dirname (current-filename))))
;;   (add-to-load-path yyuu-path-root)
;;   (add-to-load-path (string-append yyuu-path-root "/" "yyuu"))
;;   %load-path)
;; ;; load yyuu defined channels file.
;; (load-from-path "channels")


;;;; Module

(define-module (yyuu)
  #:use-module (guix profiles)		; packages->manifest
  #:use-module (gnu packages emacs-xyz) ; Emacs packages
  ;; #:use-module (yyuu channels)		; yyuu-channels
  ;; #:use-module (module
  ;;               computation
  ;;               software-engineering
  ;;               construction
  ;;               language
  ;;               programming
  ;;               haskell
  ;;               packages)
  )


;;;; Channel

;; (yyuu-channels)


;;;; Manifest

(packages->manifest
 (list
  ;; EMACS
  emacs-solarized-theme
  emacs-embark
  emacs-marginalia
  emacs-multiple-cursors
  emacs-rainbow-mode
  emacs-sudo-edit
  emacs-vertico
  emacs-which-key
  ;; emacs-fussy
  ;; emacs-perject
  ;; emacs-tabspaces
  ;; emacs-vc-use-package
  ;; emacs-winum

  ;; COMP
  emacs-geiser-guile
  emacs-guix
  emacs-smartparens

  emacs-magit

  emacs-markdown-mode

  emacs-nix-mode

  emacs-restclient

  ;;clojure
  ;;emacs-clojure-mode

  ghc
  emacs-haskell-mode

  emacs-rustic
  rust

  ;; PRO
  emacs-calibredb
  emacs-citeproc-el
  emacs-citar
  ;; emacs-citar-embark
  emacs-citar-org-roam
  emacs-org
  emacs-org-fragtog
  emacs-org-pomodoro
  emacs-org-roam
  emacs-org-roam-bibtex
  emacs-nov-el
  emacs-pdf-tools
  aspell
  aspell-dict-en
  aspell-dict-pt-br
  emacs-spell-fu

  ;; PROFILE
  emacs-elfeed

  ;; _
  emacs-emacsql-sqlite3
  emacs-emojify
  ;; emacs-emojify-logos
  emcs-vterm))


;;; manifest.scm ends here
