;;; yyuu-mod-pro-org-ys-yi.el --- ys yi: Yuu Yin's personal knowledge management (PKM)  -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Yuu Yin

;; Author: Yuu Yin <yuu@anche.no>
;; Keywords: extensions

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:


;;;; Definitions:

(defcustom yyuu-mod-pro-org-ys-yi-directory
  (expand-file-name "yi/" yyuu-mod-pro-org-ys-directory)
  "ys `yi's (Zettelkasten, PKM) directory."
  :type '(directory)
  :group 'yyuu)

(defcustom yyuu-mod-pro-org-ys-yi-yinx-file-path
  (expand-file-name "yinx.org" yyuu-mod-pro-org-ys-yi-directory)
  "ys yi `yinx's Org file path. Has notes
in the first stage of 'How to Take Smart Notes'
process (ZETTEL-1.1): capture fleeting notes. Then clarify,
organize entries in `yinx', and then `org-roam-refile'."
  :type '(file :must-match t)
  :group 'yyuu)


;;;;; Setup: capture, refile

(use-package org
  :config
  (add-to-list
   'org-capture-templates
   `("yin" "yinx" entry (file ,yyuu-mod-pro-org-ys-yi-yinx-file-path)
      "* PROCESS %^{heading}
:properties:
:id: %(org-id-new)
:timestamp_created: [%(progn (yyuu-set-org-capture-datetime-utc 'utc) (car yyuu-org-capture-datetime-utc))]
:roam_aliases:
:end:
:logbook:
- State \"PROCESS\"       from              %U
:end:

%?
"
      :prepend t
      :empty-lines 1)))


;; Footer

(provide 'yyuu-mod-pro-org-ys-yi)

;;; yyuu-mod-pro-org-ys-yi.el ends here
