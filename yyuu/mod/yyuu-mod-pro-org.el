;;; yyuu-mod-pro-org.el --- Org-mode related         -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Yuu Yin

;; Author: Yuu Yin <yuu@anche.no>
;; Keywords: extensions

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:


;;;; Directory

(use-package org
  :custom
  (org-directory
   (file-truename (getenv "ORG")))

  (org-id-locations-file
   (expand-file-name "org/org-id-locations" yy-cache-directory))

  (org-persist-directory (expand-file-name "org/org-persist/" yy-cache-directory)))

(defcustom yyuu-org-state-directory
    (expand-file-name "state/" org-directory)
    ;; (expand-file-name "state/" yy-state-directory)
    "yyuu[r] Org-mode directory for state. Must end with a slash."
    :group 'yyuu
    :type '(directory))

(defcustom yyuu-org-export-directory
  (expand-file-name "export/" org-directory)
  ;; (expand-file-name "export/" yyuu-org-state-directory)
  "Directory to export org notes."
  :type '(directory))


;;;; Habit

(use-package org
  :config
  (add-to-list 'org-modules 'habit)

  :custom
  (org-log-repeat 'lognoterepeat))


;;;; General

(use-package org
  :custom
  (org-id-track-globally t)

  (org-list-allow-alphabetical t)

  (org-log-done 'time)
  (org-log-into-drawer 'logbook)

  ;; Indentation on src blocks is useless and time consuming for copying it over
  ;; and then removing indentation.
  (org-edit-src-content-indentation 0)

  ;; FIXME: (excessive-lisp-nesting 1601)
  :hook
  (org-mode . org-indent-mode)
  (org-mode . visual-line-mode)

  :bind
  ("C-c o a" . org-agenda)
  ("C-c o c" . org-capture)
  ("C-c C-." . org-time-stamp-inactive)
  ("C-c o m" . org-mode)

  (:map org-mode-map
        ("C-c c e" . org-emphasize)
        ("C-c c i" . org-id-get-create)
        ("C-c c I" . org-id-get-create)
        ("C-c c s" . consult-org-heading)
        ("C-M-<return>" . org-insert-subheading)
        ("C-c c h" . org-toggle-heading)
        ("C-c c t i c" . org-table-insert-column)
        ("C-c c t i h" . org-table-insert-hline)
        ("C-c c t i r" . org-table-insert-row)))

;;;; TODO

(use-package org
  :custom
  (org-todo-keywords
   '((sequence "TODO(t!)"
               "REVIEW(r!)"
               "PROCESS(p!)"
               "NEXT(n!)"
               "DOING(g!)"
               "WAIT(w@/!)"
               "COMMUNICATE(c@/!)"
               "SUBMIT(s!)"  ; ready to submit
               "DELIVER(e!)" ; ready to deliver
               "LOOP(l)"     ; recurring task
               "IDEA(i!)"    ; unconfirmed and unapproved task or notion
               "MULTA(m!)"   ; multiple actions/tasks
               "MEET(M!)"    ; meeting (physical or virtual)
               "PROJ(j!)"
               "CYCLE(y)"
               "WEEK(k!)"
               "|"
               "DONE(d!)"
               "CANCEL(C@/!)"))))


;;;; Agenda

(use-package org-super-agenda
  ;; :custom
  ;; TODO: Set `org-super-agenda-groups'.
  ;; (org-super-agenda-groups '(()))

  :hook
  (after-init . org-super-agenda-mode))


;;;; Attach

(use-package org
  :custom
  ;; (org-attach-id-dir (expand-file-name "attach/" org-directory))

  ;; TODO: something like `org-attach-preferred-new-method' `dir' to share files
  ;; between all notes. So IF multiple nodes/headings need an specific file,
  ;; THEN can just attach the file to all nodes without duplicating it.
  ;; TODO: Set a method independent of below for project nodes that need
  ;; dedicated directory with ID.
  (org-attach-preferred-new-method 'id)
  (org-attach-method 'mv))


;;;; Export, publish

(use-package org
  :custom
  (org-export-initial-scope 'subtree)
  (org-export-with-priority nil)
  (org-export-with-tags nil)
  (org-export-with-tasks nil)
  (org-export-with-todo-keywords nil)
  (org-export-date-timestamp-format "%y-%m-%d")
  (org-publish-timestamp-directory (expand-file-name
                                    "org-publish-timestamp/"
                                    org-persist-directory))
  (org-publish-project-alist `(("html"
                                :base-directory ,org-directory
                                :base-extension "org"
                                :publishing-directory ,yyuu-org-export-directory
                                :publishing-function org-html-publish-to-html)
                               ("pdf"
                                :base-directory ,org-directory
                                :base-extension "org"
                                :publishing-directory ,yyuu-org-export-directory
                                :publishing-function org-latex-publish-to-pdf)
                               ("all"
                                :components ("html" "pdf"))))

  :config
  (defun yyuu-org-export-output-file-name (orig-fun extension &optional subtreep pub-dir)
    (unless pub-dir
      (setq pub-dir yyuu-org-export-directory)
      (unless (file-directory-p pub-dir)
        (make-directory pub-dir)))
    (apply orig-fun extension subtreep pub-dir nil))
  (advice-add 'org-export-output-file-name :around #'yyuu-org-export-output-file-name)

  (when (locate-library "markdown-mode")
    (add-to-list 'org-export-backends 'md)))


;;;; Notification

(use-package alert
  :custom
  (alert-fade-time 20)
  ;; `libnotify' requires `notify-send' on `PATH', and `dunst' for X OR `mako'
  ;; for Wayland.
  ;; See https://reddit.com/r/nixos/comments/ycyu9f/notifysend_in_nixos
  (alert-default-style 'libnotify))

(use-package org-wild-notifier
  :custom
  (org-wild-notifier-notification-title "YSYU")
  (org-wild-notifier-keyword-whitelist '("TODO"
                                         "REVIEW"
                                         "NEXT"
                                         "DOING"
                                         "WAIT"
                                         "COMMUNICATE"
                                         "SUBMIT"
                                         "DELIVER"
                                         "LOOP"
                                         "MEET"
                                         "PROJ"))

  :config
  (org-wild-notifier-mode 1)

  :bind
  ("C-c o N" . org-wild-notifier-mode))


;;;; TeX

(use-package texmathp)

(use-package ox-latex
  :after (org)

  :custom
  ;; FIXME: xelatex issues for org-export: xdvipdfmx:fatal: could not open
  ;; specified dvi (or xdv) file: /mnt/pro/org/export/ai.xdv
  ;; (org-latex-compiler "xelatex")
  (org-latex-compiler "lualatex"))

(use-package cdlatex
  :after (org)

  :hook
  (org-mode . turn-on-org-cdlatex))

(use-package org-latex-preview
  :after (org)

  :custom
  (org-latex-preview-default-process 'dvisvgm)

  ;; FIXME: generally problematic. when `t' and `org-latex-compiler' is
  ;; `lualatex', xcolor issues. aww, issues when `xelatex' as well when add the
  ;; microtype package.
  (org-latex-preview-use-precompilation nil)
  ;; Pre-compiles LaTeX blocks on Org-mode files.
  (org-latex-preview-precompile t)
  (org-latex-precompile t)
  (org-latex-preview-options
   (progn
     (plist-put org-latex-preview-options :background "Transparent")
     (plist-put org-format-latex-options :scale 2.0)
     (plist-put org-latex-preview-options :zoom 1.1)
     (plist-put org-latex-preview-options :matchers '("begin" "$1" "$" "$$" "\\(" "\\["))))
  (org-latex-preview-preamble "\\documentclass{article}
\[default-packages]
\[packages]
\\usepackage{xcolor}")

  :hook
  ;; fixme: buggy.
  (org-mode . org-latex-preview-auto-mode))

(use-package org-compat
  :after (org)

  :custom
  (org-latex-preview-ltxpng-directory (expand-file-name
                                       "org-latex-preview-ltxpng/"
                                       yyuu-org-state-directory)))

;; (yyuu-delete-from-list org-mode-hook 'org-latex-preview-auto-mode)


;;;; Media: image

(use-package org
  :custom
  (org-startup-with-inline-images t)
  (org-cycle-inline-images-display t))


;;;; org-pomodoro

(use-package org-pomodoro
  :custom
  (org-pomodoro-length 50)
  (org-pomodoro-short-break-length 10)
  (org-pomodoro-long-break-length 60)

  :bind
  ("C-c o p p" . org-pomodoro)
  ("C-c o p e" . org-pomodoro-extend-last-clock))


;;;; org-contacts

;; https://reddit.com/r/emacs/comments/8toivy/tip_how_to_manage_your_contacts_with_orgcontacts

(use-package org-contacts
  :custom
  (org-contacts-files `(,(expand-file-name
                         "ys/yco/yco.org"
                         org-directory)))
  (org-contacts-vcard-file (expand-file-name "ys/yco/yco.vcf" org-directory))

  :bind
  ("C-c o e" . org-contacts))


;;;; org-roam


(use-package org-roam
  :init
  ;; FIXME: https://github.com/nobiot/org-transclusion/issues/105#issuecomment-997717532
  (setq warning-suppress-types (append warning-suppress-types '((org-element-cache))))

  (use-package emacsql)
  (use-package emacsql-sqlite-builtin)

  (setq org-roam-database-connector 'sqlite-builtin)

  :config
  (cl-defmethod org-roam-node-type ((node org-roam-node))
    "Return the TYPE of NODE."
    (condition-case nil
        (file-name-nondirectory
         (directory-file-name
          (file-name-directory
           (file-relative-name (org-roam-node-file node) org-roam-directory))))
      (error "")))

  ;; https://www.emacswiki.org/emacs/RegularExpression
  ;; https://www.regextester.com/15
  ;; https://lists.gnu.org/archive/html/help-gnu-emacs/2017-05/msg00060.html
  (mapcar (lambda (regexp)
            (add-to-list 'org-roam-file-exclude-regexp
                         (concat "^" (expand-file-name org-roam-directory) "/" regexp "$")))
          '("#.*#"
            ".*~"
            "\\..*"
            "_"
            "asset"
            "blog"
            "data"
            "export"
            "ref/_"
            "state"))

  :custom
  (org-roam-directory org-directory)

  (setq org-roam-node-display-template "${title} ${tags} ${refs}")
  (org-roam-node-display-template (format "%s\t%s\t%s\t%s\t%s"
                                          "${title:*}"
                                          "${type:10}"
                                          (propertize "${tags:10}" 'face 'org-tag)
                                          "${olp:10}"
                                          "${refs}"))

  :hook
  (after-init . org-roam-db-autosync-mode)

  :bind
  ("C-c o C" . org-roam-capture)
  ("C-c o i" . org-roam-node-insert)
  ("C-c o o" . org-roam-node-find)
  ("C-c o s" . org-roam-db-sync)
  ("C-c o u" . org-roam-update-org-id-locations)
  ("C-c o t" . org-roam-alias-add)
  ("C-c o T" . org-roam-alias-remove))


;;;; Capture template: Org, Org-roam

(use-package org
  :custom
  ;; Clean capture templates for totally custom capture templates list.
  (org-capture-templates . nil))

(use-package org-roam
  :custom
  (org-roam-capture-templates . nil))

(require 'yyuu-mod-pro-org-capture)


;;;; Reference management

(use-package citeproc)

(use-package oc
  :after (org)

  :custom
  (org-cite-export-processors '((latex biblatex) (beamer natbib) (t csl))))

(use-package oc-biblatex
  :after oc)

(use-package oc-csl
  :after oc

  :custom
  (org-cite-csl-styles-dir
   (expand-file-name "asset/application/csl" org-directory))

  (org-cite-csl--fallback-style-file
   (expand-file-name "cite-iso690-numeric-en.csl" org-cite-csl-styles-dir)))

(use-package oc-natbib
  :after oc)

;; (use-package org-roam-bibtex
;;   :after
;;   org-roam

;;   :custom
;;   (orb-preformat-keywords
;;    '("=key="
;;      "author-abbrev"
;;      "author-or-editor"
;;      "author-or-editor-abbrev"
;;      "citekey"
;;      "date"
;;      "editor-abbrev"
;;      "entry-type"
;;      "file"
;;      "keywords"
;;      "note?"
;;      "pdf?"
;;      "title"
;;      "url"))

;;   ;; ORB requires this for creating and opening reference notes through orb-edit-note.
;;   (bibtex-completion-bibliography (expand-file-name "asset/text/bib.bib" org-directory))

;;   :hook
;;   (org-roam-mode . org-roam-bibtex-mode))

(use-package org
  :custom
  (org-cite-global-bibliography
   `(,(expand-file-name "asset/text/bib.bib" org-directory))))

(use-package citar
  :no-require

  :custom
  (citar-bibliography org-cite-global-bibliography)
  (citar-notes-paths `(,(expand-file-name "ref" org-directory)))
  (org-cite-insert-processor 'citar)
  (org-cite-follow-processor 'citar)
  (org-cite-activate-processor 'csl-activate)
  ;; (org-cite-activate-processor 'citar)

  :bind
  ("C-c r c" . citar-create-note)
  ("C-c o r c" . citar-create-note)
  ("C-c r o" . citar-open-note)
  ("C-c o r o" . citar-open-note)
  ("C-c r i" . citar-insert-citation)
  ("C-c o r i" . citar-insert-citation))

(use-package citar-embark
  :after citar embark
  :no-require
  :config (citar-embark-mode))

(use-package citar-org-roam
  :after
  (citar org-roam)

  :config
  (citar-org-roam-mode)

  :custom
  (citar-org-roam-subdir (expand-file-name "ref/" org-roam-directory))
  (citar-notes-source 'citar-file)
  (citar-org-roam-capture-template-key "r")
  (citar-org-roam-note-title-template "${title}"))


;;;;; ys: peronal task management and knowledge base

(require 'yyuu-mod-pro-org-ys)


;;;; Zettel, smart notes, 2nd brain

;; TODO: https://github.com/vidianos-giannitsis/zetteldesk.el#getting-started
(use-package zetteldesk)


;;;; Document integration

(use-package org-noter
  :init
  (use-package org-noter-nov)
  (use-package org-noter-pdf)
  (use-package org-noter-djvu)

  :bind
  ("C-c o n n" . org-noter)
  ("C-c o n s" . org-noter-create-skeleton))


;;;; Present

(use-package org-re-reveal
  :custom
  (org-re-reveal-root
   "file:///etc/profiles/per-user/yuu/lib/node_modules/reveal.js/")
  ;; (org-re-reveal-revealjs-version "4.4.0")
  ;; See `reveal.js/dist/theme'.
  (org-re-reveal-theme "solarized")
  (org-re-reveal-transition "convex")

  ;; (org-re-reveal-highlight-url
  ;;  "/nix/store/brnfyhk57aci344bspixzrvhmsll625y-documentation-highlighter/")
  (org-re-reveal-highlight-css 'mono-blue))

(use-package org-re-reveal-citeproc
  :config
  (add-to-list 'org-export-filter-paragraph-functions
               'org-re-reveal-citeproc-filter-cite))

;; org-present
(use-package org-present
  :defer t
  :after (org)

  :hook
  (org-present-mode . org-present-big)
  (org-present-mode . org-display-inline-images)
  (org-present-mode . org-present-hide-cursor)
  (org-present-mode . org-present-read-only)
  (org-present-mode . writeroom-mode)
  (org-present-mode-quit . org-present-small)
  (org-present-mode-quit . org-remove-inline-images)
  (org-present-mode-quit . org-present-show-cursor)
  (org-present-mode-quit . org-present-read-write)
  (org-present-mode-quit . writeroom-mode)

  :bind
  ("C-c t p" . org-present))


;;;; TODO: Org-protocol

;; (use-package )


;;;; Footer

(provide 'yyuu-mod-pro-org)

;;; yyuu-mod-pro-org.el ends here
