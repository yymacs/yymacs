;;; yyuu-mod-comp-lang-haskell.el --- yyuu's Haskell               -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Yuu Yin

;; Author: Yuu Yin <yuu@anche.no>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;


;;; Code:

(use-package haskell-mode
  :mode ("\\.hs\\'" . haskell-mode)

  :hook
  (haskell-mode . interactive-haskell-mode)

  :bind
  (:map haskell-mode-map
        ("C-c C-l" . haskell-process-load-or-reload)
        ("C-c i" . haskell-interactive-bring)
        ("C-c C-t" . haskell-process-do-type)
        ("C-c C-i" . haskell-process-do-info)
        ("C-c C-c" . haskell-process-cabal-build)
        ("C-c C-k" . haskell-interactive-mode-clear)
        ("C-c c" . haskell-process-cabal)
        ("C-`" . haskell-interactive-bring)
        ("C-c C-k" . haskell-interactive-mode-clear)
        ("C-c C-c" . haskell-process-cabal-build)
        ("C-c c" . haskell-process-cabal)))


;;;; Language Server Protocol (LSP)

(use-package eglot
  :hook
  (haskell-mode . eglot-ensure)
  (haskell-ts-mode . eglot-ensure))


(provide 'yyuu-mod-comp-lang-haskell)
;;; yyuu-mod-comp-lang-haskell.el ends here
