;; yyuu-mod-packages.el --- yyuu's packages  -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Yuu Yin

;; Author: Yuu Yin <yuu@anche.no>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:


;;;; Comp:

(yyuu-package-vc-install '(geiser-guile
                           :url "https://gitlab.com/emacs-geiser/guile.git"
                           :vc-backend Git))
(add-to-list 'package-vc-selected-packages '(julia-ts-mode
                                             :url "https://github.com/ronisbr/julia-ts-mode.git"
                                             :vc-backend Git))
(add-to-list 'package-vc-selected-packages '(eglot-jl
                                             :url "https://github.com/ronisbr/julia-ts-mode.git"
                                             :vc-backend Git))
(add-to-list 'package-vc-selected-packages '(ob-julia
                                             :url "https://github.com/karthink/ob-julia.git"
                                             :vc-backend Git))
(yyuu-package-vc-install '(haskell-mode
                           :url "https://github.com/haskell/haskell-mode.git"
                           :vc-backend Git))
(add-to-list 'package-vc-selected-packages '(lean-mode
                                             :url "https://github.com/leanprover/lean-mode.git"
                                             :vc-backend Git))

(add-to-list 'package-vc-selected-packages '(bnf-mode
                                             :url "https://github.com/sergeyklay/bnf-mode.git"
                                             :vc-backend Git))
(add-to-list 'package-vc-selected-packages '(ebnf-mode
                                             :url "https://github.com/nverno/ebnf-mode.git"
                                             :vc-backend Git))

(yyuu-package-vc-install '(magit-todos
                           :url "https://github.com/alphapapa/magit-todos.git"
                           :vc-backend Git))
(yyuu-package-vc-install '(code-review
                           :url "https://github.com/wandersoncferreira/code-review.git"
                           :vc-backend Git))
(add-to-list 'package-vc-selected-packages '(sourcegraph
                                             :url "https://github.com/danielmartin/emacs-sourcegraph.git"
                                             :vc-backend Git))

(yyuu-package-vc-install '(markdown-mode
                           :url "https://jblevins.org/projects/markdown-mode.git"
                           :vc-backend Git))
(add-to-list 'package-vc-selected-packages '(mediawiki
                                             :url "https://github.com/hexmode/mediawiki-el.git"
                                             :vc-backend Git))

(yyuu-package-vc-install '(nix-mode
                           :url "https://github.com/nixos/nix-mode.git"
                           :vc-backend Git))
(add-to-list 'package-vc-selected-packages '(nix3
                                             :url "https://github.com/emacs-twist/nix3.el.git"
                                             :vc-backend Git))

(yyuu-package-install 'csv-mode)


(yyuu-package-vc-install '(restclient
                           :url "https://github.com/pashky/restclient.el.git"
                           :vc-backend Git))
(yyuu-package-vc-install '(ob-restclient
                           :url "https://github.com/alf/ob-restclient.el.git"
                           :vc-backend Git))
(add-to-list 'package-vc-selected-packages '(graphql-mode
                                             :url "https://github.com/davazp/graphql-mode.git"
                                             :vc-backend Git))
(add-to-list 'package-vc-selected-packages '(request
                                             :url "https://github.com/tkf/emacs-request.git"
                                             :vc-backend Git)) ; for graphql-mode
(add-to-list 'package-vc-selected-packages '(ob-graphql
                                             :url "https://github.com/jdormit/ob-graphql.git"
                                             :vc-backend Git))

(add-to-list 'package-vc-selected-packages '(rustic
                           :url "https://codeberg.org/yymacs/yyel-rustic.git"
                           :branch "yy"
                           :vc-backend Git))
(add-to-list 'package-vc-selected-packages '(rust-mode
                                             :url "https://github.com/rust-lang/rust-mode.git"
                                             :vc-backend Git)) ; for rustic

(yyuu-package-vc-install '(smartparens
                           :url "https://github.com/fuco1/smartparens.git"
                           :vc-backend Git))
(yyuu-package-vc-install '(treesit-auto
                           :url "https://github.com/renzmann/treesit-auto.git"
                           :vc-backend Git))
(yyuu-package-vc-install '(envrc
                           :url "https://github.com/purcell/envrc.git"
                           :vc-backend Git))

(add-to-list 'package-vc-selected-packages '(devdocs
                                             :url "https://github.com/astoff/devdocs.el"
                                             :vc-backend Git))

(add-to-list 'package-vc-selected-packages '(ssass-mode
                                             :url "https://github.com/AdamNiederer/ssass-mode.git"
                                             :vc-backend Git))

(add-to-list 'package-vc-selected-packages '(esup
                                             :url "https://github.com/jschaf/esup.git"
                                             :vc-backend Git))

(add-to-list 'package-vc-selected-packages '(exercism
                                             :url "https://github.com/anonimitoraf/exercism.el.git"
                                             :vc-backend Git))


;;;; Emacs:

(yyuu-package-vc-install '(solarized-theme
                           :url "https://github.com/bbatsov/solarized-emacs.git"
                           :vc-backend Git))
(yyuu-package-vc-install '(avy
                           :url "https://github.com/abo-abo/avy.git"
                           :vc-backend Git))
(yyuu-package-vc-install '(link-hint
                           :url "https://github.com/noctuid/link-hint.el.git"
                           :vc-backend Git))
(yyuu-package-vc-install '(embark
                           :url "https://github.com/oantolin/embark.git"
                           :vc-backend Git))
(yyuu-package-vc-install '(embark-consult
                           :url "https://github.com/oantolin/embark.git"
                           :vc-backend Git)) ; Warning (emacs): ... for consult when embark
(yyuu-package-vc-install '(marginalia
                           :url "https://github.com/minad/marginalia.git"
                           :vc-backend Git))
(yyuu-package-vc-install '(multiple-cursors
                           :url "https://github.com/magnars/multiple-cursors.el.git"
                           :vc-backend Git))
(yyuu-package-vc-install '(phi-search
                           :url "https://github.com/zk-phi/phi-search.git"
                           :vc-backend Git)) ; for multiple-cursors
(yyuu-package-vc-install '(perject
                           :url "https://github.com/overideal/perject.git"
                           :vc-backend Git))
(yyuu-package-vc-install '(dash
                           :url "https://github.com/magnars/dash.el.git"
                           :vc-backend Git)) ; for perject
(yyuu-package-vc-install '(consult
                           :url "https://github.com/minad/consult.git"
                           :vc-backend Git)) ; for perject
(yyuu-package-install 'rainbow-mode)
(yyuu-package-vc-install '(sudo-edit
                           :url "https://github.com/nflath/sudo-edit.git"
                           :vc-backend Git))
(yyuu-package-vc-install '(vertico
                           :url "https://github.com/minad/vertico.git"
                           :vc-backend Git))
(yyuu-package-vc-install '(which-key
                           :url "https://github.com/justbur/emacs-which-key.git"
                           :vc-backend Git))
(yyuu-package-vc-install '(keycast
                           :url "https://github.com/tarsius/keycast.git"
                           :vc-backend Git))

(add-to-list 'package-vc-selected-packages '(fussy
                                             :url "https://github.com/jojojames/fussy.git"
                                             :vc-backend Git))

(add-to-list 'package-vc-selected-packages '(flx-rs
                                             :url "https://github.com/jcs-elpa/flx-rs.git"
                                             :vc-backend Git)) ; for fussy

(yyuu-package-vc-install '(orderless
                           :url "https://github.com/oantolin/orderless"
                           :vc-backend Git)) ; for fussy

(add-to-list 'package-vc-selected-packages '(prescient
                                             :url "https://github.com/radian-software/prescient.el.git"
                                             :vc-backend Git))
(yyuu-package-install 'vertico-prescient)
;; (yyuu-package-install 'company-prescient)

(add-to-list 'package-vc-selected-packages '(corfu
                                             :url "https://github.com/minad/corfu.git"
                                             :vc-backend Git))

(add-to-list 'package-vc-selected-packages '(corfu-prescient
                                             :url "https://github.com/radian-software/prescient.el.git"
                                             :main-file "corfu-prescient.el"
                                             :vc-backend Git))


(add-to-list 'package-vc-selected-packages '(corfu
                                             :url "https://github.com/minad/corfu.git"
                                             :vc-backend Git))

;; TOOD: substitute with corfu
;; (yyuu-package-vc-install '(company
;;                            :url "https://github.com/company-mode/company-mode.git"
;;                            :vc-backend Git))

;; (yyuu-package-vc-install '(company-flx
;;                            :url "https://github.com/pythonnut/company-flx.git"
;;                            :vc-backend Git))

(yyuu-package-vc-install '(vc-use-package
                           :url "https://github.com/slotThe/vc-use-package.git"
                           :vc-backend Git))
(yyuu-package-vc-install '(vterm
                           :url "https://github.com/akermu/emacs-libvterm.git"
                           :vc-backend Git))
(yyuu-package-vc-install '(winum
                           :url "https://github.com/deb0ch/winum.el.git"
                           :vc-backend Git))
(yyuu-package-vc-install '(bug-hunter
                           :url "https://github.com/malabarba/elisp-bug-hunter.git"
                           :vc-backend Git))

(add-to-list 'package-vc-selected-packages '(eat
                                             :url "https://codeberg.org/akib/emacs-eat.git"
                                             :vc-backend Git))

(add-to-list 'package-vc-selected-packages '(breadcrumb
                                             :url "https://github.com/joaotavora/breadcrumb.git"
                                             :vc-backend Git))


;;;; Pro:

(add-to-list 'package-vc-selected-packages '(elfeed
                                             :url "https://github.com/skeeto/elfeed.git"
                                             :vc-backend Git))

;; (yyuu-package-vc-install '(notmuch
;;                            :url "https://git.notmuchmail.org/git/notmuch"
;;                            :lisp-dir "emacs"
;;                            :vc-backend Git))

(yyuu-package-install 'notmuch)

(yyuu-package-vc-install '(smtpmail-multi
                           :url "https://github.com/vapniks/smtpmail-multi.git"
                           :vc-backend Git))

(yyuu-package-install 'auctex)
;; (yyuu-package-vc-install '(auctex
;;                            :url "https://git.savannah.gnu.org/git/auctex.git"
;;                            :vc-backend Git))

(yyuu-package-vc-install '(cdlatex
                           :url "https://github.com/cdominik/cdlatex.git"
                           :vc-backend Git))

(yyuu-package-vc-install '(visual-fill-column
                           :url "https://codeberg.org/joostkremers/visual-fill-column.git"
                           :vc-backend Git)) ; for writeroom-mode
(yyuu-package-vc-install '(writeroom-mode
                           :url "https://github.com/joostkremers/writeroom-mode.git"
                           :vc-backend Git))

(yyuu-package-vc-install '(emacsql-sqlite-builtin
                           :url "https://github.com/magit/emacsql.git"
                           :vc-backend Git))

(yyuu-package-vc-install '(calibredb
                           :url "https://github.com/chenyanming/calibredb.el.git"
                           :vc-backend Git))

(add-to-list 'package-vc-selected-packages '(nov
                                             :url "https://depp.brause.cc/nov.el.git"
                                             :vc-backend Git))

(yyuu-package-vc-install '(pdf-tools
                           :url "https://github.com/vedang/pdf-tools.git"
                           :lisp-dir "lisp"
                           :vc-backend Git))
(add-to-list 'package-vc-selected-packages '(org
                                             :url "https://git.tecosaur.net/tec/org-mode"
                                             :branch "dev"
                                             :lisp-dir "lisp"
                                             :vc-backend Git))



(add-to-list 'package-vc-selected-packages '(org-wild-notifier
                                             :url "https://github.com/akhramov/org-wild-notifier.el.git"
                                             :vc-backend Git))

(add-to-list 'package-vc-selected-packages '(org-contacts
                                             :url "https://repo.or.cz/org-contacts.git"
                                             :vc-backend Git))

(add-to-list 'package-vc-selected-packages '(nano-agenda
                                             :url "https://github.com/rougier/nano-agenda.git"
                                             :vc-backend Git))

(yyuu-package-vc-install '(org-super-agenda
                           :url "https://github.com/alphapapa/org-super-agenda.git"
                           :vc-backend Git))

(yyuu-package-vc-install '(org-pomodoro
                           :url "https://github.com/lolownia/org-pomodoro.git"
                           :vc-backend Git))

(yyuu-package-vc-install '(htmlize
                           :url "https://github.com/hniksic/emacs-htmlize.git"
                           :vc-backend Git)) ; for org-re-reveal
(yyuu-package-vc-install '(org-re-reveal
                           :url "https://gitlab.com/oer/org-re-reveal.git"
                           :vc-backend Git))
(yyuu-package-vc-install '(org-re-reveal-citeproc
                           :url "https://gitlab.com/oer/org-re-reveal-citeproc.git"
                           :vc-backend Git))

(add-to-list 'package-vc-selected-packages '(org-present
                                             :url "https://github.com/rlister/org-present.git"
                                             :vc-backend Git))

(yyuu-package-vc-install '(org-roam
                           :url "https://github.com/org-roam/org-roam.git"
                           :vc-backend Git))
(yyuu-package-vc-install '(org-roam-bibtex
                           :url "https://github.com/org-roam/org-roam-bibtex.git"
                           :vc-backend Git))
(yyuu-package-vc-install '(zetteldesk
                           :url "https://github.com/vidianos-giannitsis/zetteldesk.el.git"
                           :vc-backend Git))
(yyuu-package-vc-install '(org-noter
                           :url "https://github.com/c1-g/org-noter-plus-djvu.git"
                           :vc-backend Git))
(add-to-list 'load-path (expand-file-name "org-noter/modules/" package-user-dir))
(yyuu-package-vc-install '(org-pdftools
                           :url "https://github.com/fuxialexander/org-pdftools.git"
                           :vc-backend Git))


(add-to-list 'package-vc-selected-packages '(org-ai
                                             :url "https://github.com/rksm/org-ai.git"
                                             :vc-backend Git))

(yyuu-package-vc-install '(citar
                           :url "https://github.com/emacs-citar/citar.git"
                           :vc-backend Git))
(yyuu-package-vc-install '(citar-embark
                           :url "https://github.com/emacs-citar/citar.git"
                           :vc-backend Git))
(yyuu-package-vc-install '(citar-org-roam
                           :url "https://github.com/emacs-citar/citar-org-roam.git"
                           :vc-backend Git))
(yyuu-package-vc-install '(citeproc
                           :url "https://github.com/andras-simonyi/citeproc-el.git"
                           :vc-backend Git))

(add-to-list 'package-vc-selected-packages '(writegood-mode
                                             :url "https://github.com/bnbeckwith/writegood-mode"
                                             :vc-backend Git))

(add-to-list 'package-vc-selected-packages '(osm
                                             :url "https://github.com/minad/osm"
                                             :vc-backend Git))

(add-to-list 'package-vc-selected-packages '(languagetool
                                             :url "https://github.com/pillfall/languagetool.el"
                                             :vc-backend Git))

(add-to-list 'package-vc-selected-packages '(lorem-ipsum
                                             :url "https://github.com/jschaf/emacs-lorem-ipsum.git"
                                             :vc-backend Git))

(add-to-list 'package-vc-selected-packages '(powerthesaurus
                                             :url "https://github.com/savchenkovaleriy/emacs-powerthesaurus.git"
                                             :vc-backend Git))

(add-to-list 'package-vc-selected-packages '(wolfram
                                             :url "https://github.com/hsjunnesson/wolfram.el.git"
                                             :vc-backend Git))

(add-to-list 'package-vc-selected-packages '(gptel
                                             :url "https://github.com/karthink/gptel.git"
                                             :vc-backend Git))
(add-to-list 'package-vc-selected-packages '(org-ai
                                             :url "https://github.com/rksm/org-ai.git"
                                             :vc-backend Git))

(add-to-list 'package-vc-selected-packages '(starhugger
                                             :url "https://gitlab.com/daanturo/starhugger.el"
                                             :vc-backend Git))

(add-to-list 'package-vc-selected-packages '(graphviz-dot-mode
                                             :url "https://github.com/ppareit/graphviz-dot-mode.git"
                                             :vc-backend Git))

(add-to-list 'package-vc-selected-packages '(edraw
                                             :url "https://github.com/misohena/el-easydraw.git"
                                             :vc-backend Git))

(add-to-list 'package-vc-selected-packages '(polymode
                                             :url "https://github.com/polymode/polymode.git"
                                             :vc-backend Git))

(add-to-list 'package-vc-selected-packages '(poly-org
                                             :url "https://github.com/polymode/poly-org.git"
                                             :vc-backend Git))

(add-to-list 'package-vc-selected-packages '(poly-R
                                             :url "https://github.com/polymode/poly-R.git"
                                             :vc-backend Git))

(add-to-list 'package-vc-selected-packages '(plantuml-mode
                                             :url "https://github.com/skuro/plantuml-mode.git"
                                             :vc-backend Git))

(add-to-list 'package-vc-selected-packages '(ox-hugo
                                             :url "https://github.com/kaushalmodi/ox-hugo.git"
                                             :vc-backend Git))

(add-to-list 'package-vc-selected-packages '(emacs-gc-stats
                                             :url "https://git.sr.ht/~yantar92/emacs-gc-stats"
                                             :vc-backend Git))


;;;; package-vc-install

(package-vc-install-selected-packages)


;;;; Footer

(provide 'yyuu-mod-packages)

;;; yyuu-mod-packages.el ends here
