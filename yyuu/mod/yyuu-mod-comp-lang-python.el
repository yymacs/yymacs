;;; yyuu-mod-comp-lang-python.el --- Python          -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Yuu Yin

;; Author: Yuu Yin <yuu@anche.no>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:


;;;; Tree-sitter

(use-package python
  :config
  (add-to-list 'major-mode-remap-alist '(python-mode . python-ts-mode)))


;;;; Language Server Protocol (LSP)

(use-package eglot
  :hook
  (python-ts-mode . eglot-ensure)
  (python-mode . eglot-ensure))


;;;; Org-babel

(use-package ob-python
  :defer t
  :after (org))


;;;; Footer

(provide 'yyuu-mod-comp-lang-python)

;;; yyuu-mod-comp-lang-python.el ends here
