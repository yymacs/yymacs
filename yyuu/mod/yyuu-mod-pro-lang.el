;;; yyuu-mod-pro-lang.el ---                         -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Yuu Yin

;; Author: Yuu Yin <yuu@anche.no>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;


;;; Code:

;; FIXME: `jinx--load-module', (error "Jinx: Compilation of jinx-mod.so failed")
(use-package jinx
  :custom
  (jinx-languages "en en-computers en-science pt_BR")

  ;; (jinx-exclude-faces
  ;;  '(org-drawer
  ;;    org-document-info-keyword
  ;;    org-ellipsis
  ;;    org-src
  ;;    org-tag
  ;;    org-verbatim))

  :defer t
  :hook
  (text-mode . jinx-mode)
  (prog-mode . jinx-mode)
  (conf-mode . jinx-mode)

  :config (keymap-global-set "<remap> <ispell-word>" #'jinx-correct))

;; (use-package spell-fu
;;   :config
;;   (when (locate-library "org")
;;     (add-hook 'org-mode-hook
;;               (lambda ()
;;                 (setq spell-fu-faces-exclude
;;                       '(org-block-begin-line
;;                         org-block-end-line
;;                         org-code
;;                         org-date
;;                         org-drawer org-document-info-keyword
;;                         org-ellipsis
;;                         org-link
;;                         org-meta-line
;;                         org-properties
;;                         org-properties-value
;;                         org-special-keyword
;;                         org-src
;;                         org-tag
;;                         org-verbatim))
;;                 (spell-fu-mode))))

;;   :defer t
;;   :hook
;;   (spell-fu-mode . (lambda ()
;;                      (spell-fu-dictionary-add (spell-fu-get-ispell-dictionary "en"))
;;                      (spell-fu-dictionary-add (spell-fu-get-ispell-dictionary "en-computers"))
;;                      (spell-fu-dictionary-add (spell-fu-get-ispell-dictionary "en-science"))
;;                      ;; FIXME: causes 600MiB+ memory allocation to a
;;                      ;; `spell-fu-pt-br' variable, causing every buffer with
;;                      ;; spell-fu mode enabled to have that additional size.
;;                      ;; Verified with `memory-report'.
;;                      ;; https://codeberg.org/ideasman42/emacs-spell-fu/issues/40
;;                      ;; (spell-fu-dictionary-add (spell-fu-get-ispell-dictionary "pt_BR"))
;;                      ))
;;   (text-mode . spell-fu-mode)
;;   (prog-mode . spell-fu-mode))

(use-package writegood-mode
  ;; TODO: https://en.wikipedia.org/wiki/Wikipedia:Manual_of_Style/Words_to_watch
  ;; :custom
  ;; (writegood-weasels-words '())

  :hook
  (text-mode . writegood-mode)
  (org-mode . writegood-mode)
  (tex-mode . writegood-mode)
  (latex-tex-mode . writegood-mode)
  (TeX-latex-mode . writegood-mode)

  :bind
  ("C-c l w" . writegood-mode)
  ("C-c l g" . writegood-grade-level)
  ("C-c l r" . writegood-reading-ease))

(use-package languagetool
  :ensure t
  :defer t

  :commands (languagetool-check
             languagetool-clear-suggestions
             languagetool-correct-at-point
             languagetool-correct-buffer
             languagetool-set-language
             languagetool-server-mode
             languagetool-server-start
             languagetool-server-stop)
  :custom
  (languagetool-java-bin (executable-find "java"))
  (languagetool-java-arguments '("-Dfile.encoding=UTF-8"))
  (languagetool-server-command
   (expand-file-name
    "share/languagetool-server.jar"
    (file-name-directory (directory-file-name
                          (file-name-directory
                           (file-truename (executable-find
                                           "languagetool-server")))))))
  (languagetool-server-port 3829)
  (languagetool-console-command
   (expand-file-name
    "share/languagetool-commandline.jar"
    (file-name-directory (directory-file-name
                          (file-name-directory
                           (file-truename (executable-find
                                           "languagetool-commandline")))))))

  (languagetool-core-languages '(("auto" . "Automatic Detection")
                                 ("en-US" . "English (US)")
                                 ("pt-BR" . "Portuguese (Brazil)")))

  :bind
  ("C-c l m" . languagetool-server-mode)
  ("C-c l l" . languagetool-check)
  ("C-c l c" . languagetool-check)
  ("C-c l S" . languagetool-server-start)
  ("C-c l s" . languagetool-server-stop)
  ("C-c l p" . languagetool-correct-at-point)
  ("C-c l b" . languagetool-correct-buffer)
  ("C-c l k" . languagetool-clear-suggestions))

(use-package powerthesaurus
  :bind
  ("C-c l t t" . powerthesaurus-lookup-dwim)
  ("C-c l t a" . powerthesaurus-lookup-antonyms-dwim)
  ("C-c l t s" . powerthesaurus-lookup-synonyms-dwim)
  ("C-c l t r" . powerthesaurus-lookup-related-dwim)
  ("C-c l t d" . powerthesaurus-lookup-definitions-dwim)
  ("C-c l t e" . powerthesaurus-lookup-sentences-dwim))


;;;; Footer
(provide 'yyuu-mod-pro-lang)

;;; yyuu-mod-pro-lang.el ends here
