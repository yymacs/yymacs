;;; yyuu-mod-emacs-uix-theme.el --- yyuu's user interface theme  -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Yuu Yin

;; Author: Yuu Yin <yuu@anche.no>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:


;;;; Light

;;; For the built-in themes which cannot use `require'.
(use-package emacs
  ;; TODO: Set the mode-line like Tropin's.
  ;; :preface
  ;; (defun yyuu-modus-themes-custom-faces ()
  ;;   (modus-themes-with-colors
  ;;     (custom-set-faces
  ;;      ;; Add "padding" to the mode lines
  ;;      `(mode-line ((,c :box (:line-width 5 :color ,bg-mode-line-active))))
  ;;      `(mode-line-inactive ((,c :box (:line-width 5 :color ,bg-mode-line-inactive)))))))

  ;; :hook
  ;; (modus-themes-after-load-theme . yyuu-modus-themes-custom-faces)

  :custom
  (pdf-view-midnight-colors '("#000000" . "#FFFFFF"))

  :config
  (setq modus-operandi-palette-overrides
        '((bg-line-number-inactive bg-main)
          ;; WORAROUND: bg-tab-bar also sets others *-bar like menu-bar and
          ;; tool-bar.
          (bg-tab-bar bg-main)
          (border bg-main)
          (bg-mode-line-active bg-dim)
          (bg-mode-line-inactive bg-dim)
          (border-mode-line-active bg-main)
          (border-mode-line-inactive bg-main)
          (fringe bg-main)))
  (setq modus-vivendi-palette-overrides
        '((bg-line-number-inactive bg-main)
          (bg-tab-bar bg-main)
          (border bg-main)
          (bg-mode-line-active bg-dim)
          (bg-mode-line-inactive bg-dim)
          (border-mode-line-active bg-main)
          (border-mode-line-inactive bg-main)
          (fringe bg-main)))

  ;; `require-theme' is ONLY for the built-in Modus themes.
  (require-theme 'modus-themes)

  ;; Load the theme of your choice.
  (load-theme 'modus-operandi)

  :bind ("<f5>" . modus-themes-toggle))

;; (use-package solarized-theme
;;   :defer t
;;   :hook
;;   (after-init . (lambda ()
;;                   ;; (load-theme 'solarized-selenized-light t)
;;                   (load-theme 'solarized-light t)))
;;   :custom
;;   (pdf-view-midnight-colors '("#657B83" . "#FDF6E3")))


;;;; Dark

;; (use-package modus-themes
;;   :defer t
;;   :hook (after-init . (lambda () (load-theme 'modus-vivendi t))))

;; (use-package solarized-theme
;;   :defer t
;;   :hook
;;   (after-init . (lambda ()
;;                   ;; (load-theme 'solarized-selenized-dark t)
;;                   (load-theme 'solarized-dark t)))))


(provide 'yyuu-mod-emacs-uix-theme)
;;; yyuu-mod-emacs-uix-theme.el ends here
