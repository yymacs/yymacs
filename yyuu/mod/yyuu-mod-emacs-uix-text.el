;;; yyuu-mod-emacs-uix-text.el --- yyuu's user interface text   -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Yuu Yin

;; Author: Yuu Yin <yuu@anche.no>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;


;;; Code:

;;;; Definitions

(defun yyuu-sort-words (reverse beginning end)
  "Sort words in region alphabetically, in REVERSE if negative.
    Prefixed with negative \\[universal-argument], sorts in reverse.

    The variable `sort-fold-case' determines whether alphabetic case
    affects the sort order.

    See `sort-regexp-fields'."
  (interactive "*P\nr")
  (sort-regexp-fields reverse "\\w+" "\\&" beginning end))

(defun yyuu-yank-rectangle-in-new-lines ()
  "Yank the last killed rectangle in new lines."
  (interactive)
  (insert (mapconcat #'identity killed-rectangle "\n")))


;;;; Indentation / Tabs

(setq-default indent-tabs-mode nil)


;;;; Whitespace

;; Clean whitespace on save.
(use-package whitespace
  :ensure nil
  :hook
  (before-save . whitespace-cleanup))

;;;; Eval
(setq-default eval-expression-print-length nil
              eval-expression-print-level nil
              print-length nil
              print-level nil)


(provide 'yyuu-mod-emacs-uix-text)
;;; yyuu-mod-emacs-uix-text.el ends here
