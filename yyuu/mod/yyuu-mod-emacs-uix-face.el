;;; yyuu-mod-emacs-uix-face.el --- yyuu's user interface face   -*- lexical-binding: t; -*-

;; Copyright (C) 2022--2023  Yuu Yin

;; Author: Yuu Yin <yuu@anche.no>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(require 'yyuu-mod-lib)

;; Default font (cant be font with hyphen in the name like Inconsolata-g)
(setq initial-frame-alist '((font . "Iosevka Fixed"))
      default-frame-alist '((font . "Iosevka Fixed")))

;; Emoji: 😄, 🤦, 🏴, , 
;; Should render as 3 color emojis and 2 glyphs
(defun yyuu-set-fonts()
 "Set the emoji and glyph fonts."
  (set-fontset-font t 'symbol "Iosevka Fixed 14" nil 'prepend)
  (set-fontset-font t 'symbol "Twitter Color Emoji 16" nil 'prepend))

;; Respect default terminal fonts if we're in a gui set the fonts appropriately
;; for daemon sessions and and nondaemons.
(yyuu-apply-if-gui 'yyuu-set-fonts)

;; (custom-set-faces `(default ((t (:height 180))))
;;                   `(fixed-pitch ((t (:inherit (default)))))
;;                   `(fixed-pitch-serif ((t (:inherit (default))))))


;; Highlights color code's background. Useful for programming.
(use-package rainbow-mode
  :hook
  (prog-mode . rainbow-turn-on)
  (conf-mode . rainbow-turn-on))


(provide 'yyuu-mod-emacs-uix-face)
;;; yyuu-mod-emacs-uix-face.el ends here
