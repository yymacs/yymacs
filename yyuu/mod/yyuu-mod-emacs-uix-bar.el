;;; yyuu-mod-emacs-uix-bar.el ---                               -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Yuu Yin

;; Author: Yuu Yin <yuu@anche.no>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;


;;; Code:


;;;; Menubar

;; WORKAROUND: Set to `t' because xmonad-contexts' issue as a top bar when in
;; tabbed layout, so top content in GNU Emacs is not visible. This helps so only
;; menu-bar, which I do not use, becomes non-visible. Actually, the xmonad
;; contexts bar does not over it, but ...?
(customize-set-variable 'menu-bar-mode t)


;;;; Toolbar

(customize-set-variable 'tool-bar-mode nil)


;;;; Scrollbar

(customize-set-variable 'scroll-bar-mode nil)


(provide 'yyuu-mod-emacs-uix-bar)
;;; yyuu-mod-emacs-uix-bar.el ends here
