;;; yyuu-mod-comp-conf-manage.el --- yyuu's configuration management  -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Yuu Yin

;; Author: Yuu Yin <yuu@anche.no>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;


;;; Code:


;;;; Version Control System (VCS)

(use-package magit
  :custom
  (magit-git-executable (file-truename (executable-find "git")))
  (magit-perl-executable (file-truename (executable-find "perl"))))

(use-package magit-todos
  :requires
  (magit)

  :after
  (magit)

  :config
  (add-to-list 'magit-todos-keywords-list "FEATURE")
  (add-to-list 'magit-todos-keywords-list "PATCH")

  ;; TODO: Set caching options.
  ;; :custom

  :hook
  (magit-mode . magit-todos-mode))


;;;;; Forge

;; TODO: Configure forge.
(use-package forge
  ;; yaml.el: Error: error Invalid escape character syntax
  :defer t

  :after
  (magit))

(use-package code-review
  :custom
  (code-review-auth-login-marker 'code-review)
  (code-review-fill-column 80)
  (code-review-download-dir "/tmp/yy/code-review/")

  :bind
  ("C-c d r" . code-review-start))


;;;; direnv

(use-package envrc
  :custom
  (envrc-direnv-executable (file-truename (executable-find "direnv")))

  :config
  (envrc-global-mode)

  :bind
  ("C-c d v a" . envrc-allow)
  ("C-c d v d" . envrc-deny)
  ("C-c d v r" . envrc-reload)
  ("C-c d v R" . envrc-reload-all))


;;;; Diff

(use-package diff-mode
  :defer t
  :hook
  (diff-mode . (lambda () (setq-local truncate-lines t))))

(use-package ediff
  :custom
  (ediff-window-setup-function #'ediff-setup-windows-plain)
  (ediff-split-window-function #'split-window-horizontally))


;;;; Search

(use-package sourcegraph
  :defer t

  :bind
  ("C-c d s" . sourcegraph-search))


(provide 'yyuu-mod-comp-conf-manage)
;;; yyuu-mod-comp-conf-manage.el ends here
