;;; yyuu-mod-emacs-uix-input-key.el --- UI/UX input key  -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Yuu Yin

;; Author: Yuu Yin <yuu@anche.no>
;; Keywords: extensions

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

;; TODO: see if can require in below.
(use-package picture)

(use-package emacs
  :bind
  ("C-x r R" . bookmark-relocate)

  ("C-c a s" . yyuu-screenshot-svg)

  ("M-[" . join-line)
  ("C-x r Y" . yyuu-yank-rectangle-in-new-lines)
  ("C-c o l" . calendar)

  ("C-h D" . shortdoc-display-group)

  ;; Dev (d)
  ("C-c d C" . compile)
  ("C-c d I" . auto-insert)
  ("C-c d E" . eshell)

  ;; Emacs (e)
  ("C-c e G" . garbage-collect)
  ("C-c e b" . butterfly)
  ("C-c e f" . find-library)
  ("C-c e g" . customize-group)
  ("C-c e K" . kill-emacs)
  ("C-c e l" . load-library)
  ("C-c e L" . unload-feature)
  ("C-c e m" . memory-report)
  ("C-c e o" . customize-option)
  ("C-c e s" . profiler-start)
  ("C-c e S" . profiler-stop)
  ("C-c e r" . profiler-report)

  ;; Toggle (t)
  ("C-c t d" . toggle-debug-on-error)
  ("C-c t l" . visual-line-mode)
  ("C-c t o" . overwrite-mode)
  ("C-c t r" . rainbow-mode)
  ("C-c t t" . toggle-truncate-lines)

  ;; File (f)
  (:prefix-map
   file
   :prefix "C-c f"
   ("C" . make-empty-file)
   ("c" . copy-file)
   ("D" . delete-file)
   ("d" . mkdir)
   ("e" . ediff-files)
   ("E" . ediff3-files)
   ("i" . insert-file)
   ("l" . load-file)
   ("m" . rename-file)
   ("f" . recentf-open)
   ("r o" . recentf-open)
   ("F" . recentf-open-files)
   ("r O" . recentf-open-files)
   ("r f" . recentf-open)
   ("r r" . recentf-open)
   ("r s" . recentf-save-list)
   ("r e" . recentf-edit-list)
   ("y" . make-empty-file)
   ("Y" . dired-create-empty-file))

  ;; Text (x)
  (:prefix-map
   text
   :prefix "C-c x"
   ("a a" . align)
   ("a c" . align-current)
   ("a e" . align-entire)
   ("a i" . auto-insert)
   ("a r" . align-regexp)
   ("a t" . tabify)
   ("a s" . untabify)
   ("a T" . untabify)
   ("a u" . untabify)
   ("c l a" . link-hint-copy-all-links)
   ("c l l" . link-hint-copy-link-at-point)
   ("c l L" . link-hint-copy-link)
   ("c l m" . link-hint-copy-multiple-links)
   ("d l" . picture-duplicate-line)
   ("i a" . auto-insert)
   ("i d" . yyuu-insert-datetime-utc)
   ("l j" . join-line)
   ("l s" . sort-lines)
   ("l d" . picture-duplicate-line)
   ("i l" . lorem-ipsum-insert-list)
   ("i h" . lorem-ipsum-insert-list)
   ("L l" . lorem-ipsum-insert-list)
   ("L h" . lorem-ipsum-insert-list)
   ("i p" . lorem-ipsum-insert-paragraphs)
   ("L p" . lorem-ipsum-insert-paragraphs)
   ("i s" . lorem-ipsum-insert-sentences)
   ("L s" . lorem-ipsum-insert-sentences)
   ("s r" . sort-regexp-fields)
   ("s l" . sort-lines)
   ("s w" . yyuu-sort-words)
   ("w s" . yyuu-sort-words)))


;;; Footer

(provide 'yyuu-mod-emacs-uix-input-key)

;;; yyuu-mod-emacs-uix-input-key.el ends here
