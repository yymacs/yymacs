;;; yyuu-mod-emacs-uix.el --- yyuu's user interface and experience  -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Yuu Yin

;; Author: Yuu Yin <yuu@anche.no>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:


;;;; Help, documentation

(use-package woman
  :bind
  (("C-h W" . woman)))


;;;; Frame

(setq frame-resize-pixelwise t)

;;;; Tab-bar

(use-package emacs
  :config
  (setq-default tab-bar-show nil))


;;;; Feedback

(use-package breadcrumb
  :config
  (breadcrumb-mode))


;;;; Fringe

;; (use-package emacs
;;   :preface
;;   (defcustom yyuu-fringe-width nil
;;     "Fringe's width for both left and right sides."
;;     :group 'yyuu
;;     :type '(num))

;;   :custom
;;   (left-fringe-width yyuu-fringe-width)
;;   (right-fringe-width yyuu-fringe-width))


;;;; Scroll

;; OwO, this is nice.
(use-package pixel-scroll
  :custom
  (pixel-scroll-precision-interpolate-page t)

  :hook
  (after-init . pixel-scroll-precision-mode)

  :bind
  ;; FIXME: `pixel-scroll-interpolate' does not work on `minibuffer-mode'. So in
  ;; the `Minibuffer' section on the `buffer' module, yyuu sets `scroll-command'
  ;; for `minibuffer-mode' instead.
  ("C-v" . pixel-scroll-interpolate-down)
  ("M-v" . pixel-scroll-interpolate-up)
  ("M-n" . pixel-scroll-up)
  ("M-p" . pixel-scroll-down))

;; TODO: Refactor, prettify.
;; FIX: When cursor is buffer's last line.
;; FIX: In Magit.
;; https://matrix.to/#/!WfZsmtnxbxTdoYPkaT:greyface.org/$7eT577ASAXcHL6mQ375ulOS_-sLfvFGCMsdF0DsfIuc?via=matrix.org&via=envs.net&via=kde.org
(use-package pixel-scroll
  :config
  (defun yantar-recenter (&optional arg redisplay)
    "Like `recenter' but use smooth scroll."
    (pcase arg
      (`nil
       ;; Scroll smoothly, with line precision.
       (ignore-errors
         (pixel-scroll-precision-interpolate
          (* (line-pixel-height)
             (- (/ (count-lines (window-start) (window-end)) 2)
                (count-lines (window-start) (point))))
          nil 1))
       ;; Call original recenter for final adjustment.
       (recenter arg redisplay))
      ((pred (not numberp))
       (recenter arg redisplay))
      ((pred (<= 0))
       ;; Scroll smoothly, with line precision.
       (ignore-errors
         (pixel-scroll-precision-interpolate
          (* -1 (line-pixel-height)
             (max 0 (- (count-lines (window-start) (point)) 2 arg)))
          nil 1))
       ;; Call original recenter for final adjustment.
       (recenter arg redisplay))
      ((pred (> 0))
       ;; Scroll smoothly, with line precision.
       (ignore-errors
         (pixel-scroll-precision-interpolate
          (* (line-pixel-height)
             (max 0 (- (count-lines (point) (window-end)) 3 arg)))
          nil 1))
       ;; Call original recenter for final adjustment.
       (recenter arg redisplay))))

  (defun yantar-recenter-top-bottom-pixel (&optional arg)
    "Like `recenter-top-bottom' but use smooth scrolling."
    (interactive "P")
    (cond
     (arg (yantar-recenter arg t))                 ; Always respect ARG.
     (t
      (setq recenter-last-op
            (if (eq this-command last-command)
                (car (or (cdr (member recenter-last-op recenter-positions))
                         recenter-positions))
              (car recenter-positions)))
      (let ((this-scroll-margin
             (min (max 0 scroll-margin)
                  (truncate (/ (window-body-height) 4.0)))))
        (cond ((eq recenter-last-op 'middle)
               (yantar-recenter nil t))
              ((eq recenter-last-op 'top)
               (yantar-recenter this-scroll-margin t))
              ((eq recenter-last-op 'bottom)
               (yantar-recenter (- -1 this-scroll-margin) t))
              ((integerp recenter-last-op)
               (yantar-recenter recenter-last-op t))
              ((floatp recenter-last-op)
               (yantar-recenter (round (* recenter-last-op (window-height))) t)))))))

  :bind
  ;; ("C-l" . yantar-recenter-top-bottom-pixel)
  ("C-l" . recenter-top-bottom)
  ("C-M-]" . reposition-window)
  ("C-M-}" . recenter-other-window))


;;;; Navigation

(use-package avy
  :bind
  ("M-g c" . avy-goto-char)
  ("M-g M-g" . avy-goto-line)
  ("M-g g" . avy-goto-word-0))

(use-package embark
  :ensure t

  :bind
  (("M-g a" . embark-act)
   ("C-c a a" . embark-act)     ;; pick some comfortable binding
   ("C-c a d" . embark-dwim)    ;; good alternative: M-.
   ("C-h B" . embark-bindings)) ;; alternative for `describe-bindings'

  ;; :init
  ;; Optionally replace the key help with a completing-read interface
  ;; (setq prefix-help-command #'embark-prefix-help-command)

  :config
  ;; Hide the mode line of the Embark live/completions buffers
  (add-to-list 'display-buffer-alist
               '("\\`\\*Embark Collect \\(Live\\|Completions\\)\\*"
                 nil
                 (window-parameters (mode-line-format . none)))))

;; Consult users will also want the embark-consult package.
(use-package embark-consult
  :ensure t ; only need to install it, embark loads it after consult if found
  :hook
  (embark-collect-mode . consult-preview-at-point-mode))

(use-package consult
  ;; Enable automatic preview at point in the *Completions* buffer. This is
  ;; relevant when you use the default completion UI.
  :hook
  (completion-list-mode . consult-preview-at-point-mode)

  ;; The :init configuration is always executed (not lazy).
  :init

  ;; Optionally configure the register formatting. This improves the register
  ;; preview for `consult-register', `consult-register-load',
  ;; `consult-register-store' and the Emacs built-ins.
  (setq register-preview-delay 0.5
        register-preview-function #'consult-register-format)

  ;; Optionally tweak the register preview window. This adds thin lines, sorting
  ;; and hides the mode line of the window.
  (advice-add #'register-preview :override #'consult-register-window)

  ;; Configure other variables and modes in the :config section, after lazily
  ;; loading the package.

  :config
  ;; Optionally configure preview. The default value
  ;; is 'any, such that any key triggers the preview.
  ;; (setq consult-preview-key 'any)
  ;; (setq consult-preview-key "M-.")
  ;; (setq consult-preview-key '("S-<down>" "S-<up>"))
  ;; For some commands and buffer sources it is useful to configure the
  ;; :preview-key on a per-command basis using the `consult-customize' macro.
  (consult-customize
   consult-theme :preview-key '(:debounce 0.2 any)
   consult-ripgrep consult-git-grep consult-grep
   consult-bookmark consult-recent-file consult-xref
   consult--source-bookmark consult--source-file-register
   consult--source-recent-file consult--source-project-recent-file
   ;; :preview-key "M-."
   :preview-key '(:debounce 0.4 any))

  ;; Optionally configure the narrowing key.
  ;; Both < and C-+ work reasonably well.
  (setq consult-narrow-key "<") ;; "C-+"

  ;; Optionally make narrowing help available in the minibuffer.
  ;; You may want to use `embark-prefix-help-command' or which-key instead.
  ;; (define-key consult-narrow-map (vconcat consult-narrow-key "?") #'consult-narrow-help)

  ;; Replace bindings. Lazily loaded due by `use-package'.
  :bind (;; C-c bindings (mode-specific-map)
         ("C-c M-x" . consult-mode-command)
         ("C-c a h" . consult-history)
         ("C-c a k" . consult-kmacro)
         ("C-c a m" . consult-man)
         ("C-c a i" . consult-info)
         ;; ([remap Info-search] . consult-info)
         ;; C-x bindings (ctl-x-map)
         ("C-x M-:" . consult-complex-command)     ;; orig. repeat-complex-command
         ("C-x b" . consult-buffer)                ;; orig. switch-to-buffer
         ("C-x B" . switch-to-buffer)
         ;; ("C-x 4 b" . consult-buffer-other-window) ;; orig. switch-to-buffer-other-window
         ;; ("C-x 5 b" . consult-buffer-other-frame)  ;; orig. switch-to-buffer-other-frame
         ;; ("C-x r b" . consult-bookmark)            ;; orig. bookmark-jump
         ;; ("C-x p b" . consult-project-buffer)      ;; orig. project-switch-to-buffer
         ;; Custom M-# bindings for fast register access
         ;; ("M-#" . consult-register-load)
         ;; ("M-'" . consult-register-store)          ;; orig. abbrev-prefix-mark (unrelated)
         ;; ("C-M-#" . consult-register)
         ;; Other custom bindings
         ;; ("M-y" . consult-yank-pop)                ;; orig. yank-pop
         ;; M-g bindings (goto-map)
         ("M-g e" . consult-compile-error)
         ("M-g f" . consult-flymake)               ;; Alternative: consult-flycheck
         ("M-g l" . consult-goto-line)           ;; orig. goto-line
         ("M-g o" . consult-outline)
         ("M-g O" . consult-org-heading)
         ("M-g m" . consult-mark)
         ("M-g k" . consult-global-mark)
         ("M-g i" . consult-imenu)
         ("M-g I" . consult-imenu-multi)
         ;; M-s bindings (search-map)
         ("C-c f g" . consult-find)
         ("M-s f" . consult-find)
         ("M-s D" . consult-locate)
         ("M-s g" . consult-ripgrep)
         ("M-s G" . consult-git-grep)
         ("M-s s" . consult-line)
         ("M-s M-s" . consult-line)
         ("M-s S" . consult-line-multi)
         ("M-s M-S" . consult-line-multi)
         ("M-s k" . consult-keep-lines)
         ("M-s u" . consult-focus-lines)
         ;; Isearch integration
         ("M-s e" . consult-isearch-history)
         (:map isearch-mode-map
               ("M-e" . consult-isearch-history)         ;; orig. isearch-edit-string
               ("M-s e" . consult-isearch-history)       ;; orig. isearch-edit-string
               ("M-s l" . consult-line)                  ;; needed by consult-line to detect isearch
               ("M-s L" . consult-line-multi))           ;; needed by consult-line to detect isearch
         ;; Minibuffer history
         (:map minibuffer-local-map
               ("M-s" . consult-history)                 ;; orig. next-matching-history-element
               ("M-r" . consult-history))))       ;; orig. previous-matching-history-element

(use-package consult-xref
  :after
  (consult)

  :custom
  ;; Use Consult to select xref locations with preview
  (xref-show-xrefs-function #'consult-xref)
  (xref-show-definitions-function #'consult-xref))


;;;; Unbackup

;; Do not generate backups or lockfiles. While auto-save maintains a copy so
;; long as a buffer is unsaved, backups create copies once, when the file is
;; first written, and never again until it is killed and reopened. This is
;; better suited to version control, and I don't want world-readable copies of
;; potentially sensitive material floating around our filesystem.
(setq create-lockfiles nil
      make-backup-files nil
      ;; But in case the user does enable it, some sensible defaults:
      version-control t     ; number each backup file
      backup-by-copying t   ; instead of renaming current file (clobbers links)
      delete-old-versions t ; clean up after itself
      kept-old-versions 5
      kept-new-versions 5
      backup-directory-alist (list (cons "." (concat yy-cache-directory "backup/")))
      tramp-backup-directory-alist backup-directory-alist)


;;;; Unannoy

;; UX: Suppress compiler warnings and do not inundate users with their popups.
;;   They are rarely more than warnings, so are safe to ignore.
(setq native-comp-async-report-warnings-errors init-file-debug
      native-comp-warning-on-missing-source init-file-debug)

(customize-set-variable 'initial-scratch-message nil)


;;;; Date-Time

(use-package time
  :custom
  (display-time-day-and-date t)
  (display-time-24hr-format t)
  (display-time-format "(%a %FT%T)")

  :config
  (display-time))


;;;; Temporary

;; FIX: Emacs fails saving GPG encrypted files.
;; https://www.masteringemacs.org/article/keeping-secrets-in-emacs-gnupg-auth-sources
;; https://emacs.stackexchange.com/a/78896
(use-package epg
  :config
  (fset 'epg-wait-for-status 'ignore))


;;; Footer

(provide 'yyuu-mod-emacs-uix)

;;; yyuu-mod-emacs-uix.el ends here
