;;; yyuu-mod-comp-lang-julia.el --- Julia programming language  -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Yuu Yin

;; Author: Yuu Yin <yuu@anche.no>
;; Keywords: extensions

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:


;;;; Tree-sitter

(use-package julia-ts-mode
  :config
  (add-to-list 'major-mode-remap-alist '(julia-mode . julia-ts-mode)))


;;;; Language Server Protocol (LSP)

;; https://github.com/julia-vscode/languageserver.jl
(use-package eglot-jl
  :hook
  (julia-ts-mode . eglot-ensure)
  (julia-mode . eglot-ensure))


;;;; Org Babel

(use-package ob-julia
  :after (org)
  :defer t

  :commands org-babel-execute:julia

  :custom
  (ob-julia-startup-script (expand-file-name
                            "ob-julia/julia/init.jl" package-user-dir))

  (org-babel-julia-command-arguments '("--threads=2"
                                       "--banner=no")))


(provide 'yyuu-mod-comp-lang-julia)
;;; yyuu-mod-comp-lang-julia.el ends here
