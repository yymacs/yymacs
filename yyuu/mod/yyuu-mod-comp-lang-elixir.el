;;; yyuu-mod-comp-lang-elixir.el --- Elixir programming language -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Yuu Yin

;; Author: Yuu Yin <yuu@anche.no>
;; Keywords: extensions

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:


;;;; Tree-sitter

(use-package elixir-ts-mode
  :config
  (add-to-list 'major-mode-remap-alist '(elixir-mode . elixir-ts-mode))
  (add-to-list 'auto-mode-alist '("\\.exs\\'" . elixir-ts-mode)))


;;;; Language Server Protocol (LSP)

(use-package eglot
  :hook
  (elixir-ts-mode . eglot-ensure))


;;; Footer

(provide 'yyuu-mod-comp-lang-elixir)

;;; yyuu-mod-comp-lang-elixir.el ends here
