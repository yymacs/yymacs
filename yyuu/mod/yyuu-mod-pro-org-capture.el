;;; yyuu-mod-pro-org-capture.el ---                   -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Yuu Yin

;; Author: Yuu Yin <yuu@anche.no>
;; Keywords: extensions

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:


;;;; Definition


;;;;; Variable

(defcustom yyuu-org-capture-datetime-utc nil
  "Datetime for timestaping and uniquely naming Org notes."
  :group 'yyuu
  :type '(sexp))


;;;;; Function

(defun yyuu-set-org-capture-datetime-utc (&optional format)
  (customize-set-variable 'yyuu-org-capture-datetime-utc
                          (yyuu-datetime-utc-list))
  (cond
   ((eq format 'utc) (car (last yyuu-org-capture-datetime-utc)))
   ((eq format 'iso) (car yyuu-org-capture-datetime-utc))
   (t (car (last yyuu-org-capture-datetime-utc)))))

(use-package org-roam
  :config
  (cl-defmethod org-roam-node-slug ((node org-roam-node))
  "Return the slug of NODE."
  (let ((title (org-roam-node-title node))
        (slug-trim-chars '(;; Combining Diacritical Marks https://www.unicode.org/charts/PDF/U0300.pdf
                           768    ; U+0300 COMBINING GRAVE ACCENT
                           769    ; U+0301 COMBINING ACUTE ACCENT
                           770    ; U+0302 COMBINING CIRCUMFLEX ACCENT
                           771    ; U+0303 COMBINING TILDE
                           772    ; U+0304 COMBINING MACRON
                           774    ; U+0306 COMBINING BREVE
                           775    ; U+0307 COMBINING DOT ABOVE
                           776    ; U+0308 COMBINING DIAERESIS
                           777    ; U+0309 COMBINING HOOK ABOVE
                           778    ; U+030A COMBINING RING ABOVE
                           779    ; U+030B COMBINING DOUBLE ACUTE ACCENT
                           780    ; U+030C COMBINING CARON
                           795    ; U+031B COMBINING HORN
                           803    ; U+0323 COMBINING DOT BELOW
                           804    ; U+0324 COMBINING DIAERESIS BELOW
                           805    ; U+0325 COMBINING RING BELOW
                           807    ; U+0327 COMBINING CEDILLA
                           813    ; U+032D COMBINING CIRCUMFLEX ACCENT BELOW
                           814    ; U+032E COMBINING BREVE BELOW
                           816    ; U+0330 COMBINING TILDE BELOW
                           817))) ; U+0331 COMBINING MACRON BELOW
    (cl-flet* ((nonspacing-mark-p (char) (memq char slug-trim-chars))
               (strip-nonspacing-marks (s) (string-glyph-compose
                                            (apply #'string
                                                   (seq-remove #'nonspacing-mark-p
                                                               (string-glyph-decompose s)))))
               (cl-replace (title pair) (replace-regexp-in-string (car pair) (cdr pair) title)))
      (let* ((pairs `(("[^[:alnum:][:digit:]]" . "-") ;; convert anything not alphanumeric
                      ("--*" . "-")                   ;; remove sequential hyphens
                      ("^-" . "")                     ;; remove starting hyphen
                      ("-$" . "")))                   ;; remove ending hyphen
             (slug (-reduce-from #'cl-replace (strip-nonspacing-marks title) pairs)))

        (downcase slug))))))


;;;; Customization

;;;;; Org-roam capture

(use-package org-roam
  :config
  (add-to-list 'org-roam-capture-templates
               (backquote ("d" "default" plain "%?"
                           :target (file+head "${slug}-%(yyuu-set-org-capture-datetime-utc 'utc).org"
                                              "
:properties:
:id: ${id}
:timestamp_created: [%(car yyuu-org-capture-datetime-utc)]
:roam_aliases: %^{aliases}
:end:

#+title: ${title}
#+category:
#+filetags:

#+setupfile: setupfile.org
#+bibliography: asset/text/bib.bib

- categories ::
- subcategories ::

* what

* more

* references
#+print_bibliography:
")
                           :unnarrowed t)))
  (add-to-list 'org-roam-capture-templates
               (backquote ("r" "reference" plain
      "%?"
      :target
      (file+head
       "%(expand-file-name (or citar-org-roam-subdir \"\") org-roam-directory)/${citar-citekey}.org"
       "
:properties:
:id: ${id}
:timestamp_created: %U
:timestamp_updated: %U
:roam_refs: @${citar-citekey}
:roam_aliases: ${citar-citekey}
:end:

#+title: ${citar-citekey} ${title}
#+category:
#+filetags: :reference:yee:

#+bibliography: asset/text/bib.bib

#+seq_todo: TODO PREVIEW ASK READ RECORD RELATE RECITE REPEAT PRACTICE REVIEW | DONE CANCELED

- categories ::
- subcategories ::

* ${title}
:properties:
:id_custom: ${citar-citekey}
:author: ${citar-author}
:date: ${citar-date}
:noter_document:
:noter_page:
:end:

** notes

*** keywords

*** questions

*** own words
")
      :unnarrowed t)))

  :custom
  (org-roam-dailies-directory org-directory)
  (org-roam-dailies-capture-templates
   '(("d" "default" entry
"
\n
* %<%T%:z>: %?
:properties:
:id: ${id}
:timestamp_created: [%(yyuu-org-capture-datetime-utc 'iso)]
:end:
"
      :if-new (file+head "%<%y%m%d>.org"
"
:properties:
:id: ${id}
:timestamp_created: [%(yyuu-org-capture-datetime-utc 'iso)]
:end:

#+title: %<%y-%m-%d>
#+category:
#+filetags: :daily:

#+setupfile: ../setupfile.org
#+bibliography: ../asset/text/bib.bib

- categories ::
- subcategories ::
")))))


;;;; Footer

(provide 'yyuu-mod-pro-org-capture)

;;; yyuu-mod-pro-org-capture.el ends here
