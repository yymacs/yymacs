;;; yyuu-mod-pro-tex.el --- TeX, LaTeX, BibTeX       -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Yuu Yin

;; Author: Yuu Yin <yuu@anche.no>
;; Keywords: extensions

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:


;;;; General

(use-package tex
  :defer t
  :ensure auctex

  :custom
  ;; The compiler, such as`pdflatex',`xelatex',`lualatex', for producing the
  ;; PDF.
  (latex-run-command "lualatex")
  (LaTeX-command "xelatex")

  ;; Enable document parsing for many LaTeX packages.
  (TeX-auto-save t)
  (TeX-parse-self t)

  :config
  ;; Support for `\include' or `\input'; Each time you open a new file, AUCTeX
  ;; will then ask you for a master file.
  (setq-default TeX-master nil)

  (add-to-list 'major-mode-remap-alist '(tex-mode . TeX-latex-mode)))


;;;; Auto input

;; (use-package auctex
;;  :custom
;;  ;; Enable document parsing for many LaTeX packages.
;;  (TeX-auto-save t)
;;  (TeX-parse-self t)

;;  :config
;;  ;; Support for `\include' or `\input'; Each time you open a new file, AUCTeX
;;  ;; will then ask you for a master file.
;;  (setq-default TeX-master nil)

;;  :mode ("\\.tex\\'" . TeX-latex-mode))


;;;; Reference management

(use-package reftex
  ;; :custom
  ;; (reftex-label-alist . '(("axiom"   ?a "ax:"  "~\\ref{%s}" nil ("axiom"   "ax.") -2)
  ;;                         ("theorem" ?h "thr:" "~\\ref{%s}" t   ("theorem" "th.") -3)))

  :hook
  (LaTeX-mode . turn-on-reftex)
  (latex-mode . turn-on-reftex))

;; Fast insertion of environment templates and math stuff in LaTeX.
;; Usage: `abbreviation<TAB>' (e.g. `ite<TAB>').
(use-package cdlatex
  :hook
  ;; with AUCTeX LaTeX mode
  (LaTeX-mode . turn-on-cdlatex)
  ;; with Emacs latex mode
  (latex-mode . turn-on-cdlatex))


;;;; Preview

;; (use-package latex-preview-pane
;;   :config
;;   (latex-preview-pane-enable))


(provide 'yyuu-mod-pro-tex)
;;; yyuu-mod-pro-tex.el ends here
