;;; yyuu-mod-emacs-uix-input.el --- yyuu's user interface input  -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Yuu Yin

;; Author: Yuu Yin <yuu@anche.no>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;


;;; Code:


;;;; Cursor

(use-package multiple-cursors
  :bind
  ("C->" . mc/mark-next-like-this)
  ("C-<" . mc/mark-previous-like-this)
  ("C-{" . mc/unmark-next-like-this)
  ("C-}" . mc/unmark-previous-like-this))


;;;; Auto-insert

(use-package smartparens
  :config
  (smartparens-global-mode))

;; TODO: substitute smartparens with electric pair
;; (use-package elec-pair)

(use-package auto-insert
  :defer t
  :custom
  (auto-insert-directory (expand-file-name
                          "yyuu-mod-emacs-uix-auto-insert/"
                          yyuu-mod-directory))

  :config
  (auto-insert-mode))


;;;; Prompt

(use-package emacs
  :custom
  ;; Prompt/Ask `y or n' instead of `yes or no`.
  (use-short-answers t))


;;;; Template

(use-package tempo
  :config
  (defvar yyuu-tempo-tags-latex nil
    "Tempo tags for LaTeX mode.")
  (defvar yyuu-tempo-tags-org nil
    "Tempo tags for Org mode.")

  (tempo-define-template "rs"
                         '("#+begin_src rust" > n>
                           r> n>
                           "#+end_src")
                         "rs"
                         "Insert an Org src block for Rust"
                         ;; 'yyuu-tempo-tags-org
                         )

  (tempo-define-template "rsm"
                         '("#+begin_src rust" > n>
                           "fn main() {" > n>
                           r> n>
                           "}" > n>
                           "#+end_src")
                         "rsm"
                         "Insert an Org src block for Rust"
                         ;; 'yyuu-tempo-tags-org
                         )

  (tempo-define-template "rso"
                         '("#+begin_src rust :results output" > n>
                           "fn main() {" > n>
                           r> n>
                           "}" > n>
                           "#+end_src")
                         "rso"
                         "Insert an Org src block for Rust"
                         ;; 'yyuu-tempo-tags-org
                         )

  (tempo-define-template "begin-environment"
                         '("\\begin{" (p "Environment: " environment) "}" > n>
                           r> n>
                           "\\end{" (s environment) "}" > n)
                         "lenv"
                         "Insert a LaTeX environment."
                         ;; 'yyuu-tempo-tags-latex
                         )

  (tempo-define-template "math"
                         '("\\begin{math}" > n>
                           r> n>
                           "\\end{math}" > n>)
                         "math"
                         "Insert a LaTeX for math"
                         ;; 'yyuu-tempo-tags-tex
                         )

  (tempo-define-template "equation"
                         '("\\begin{equation}" > n>
                           r> n>
                           "\\end{equation}" > n>)
                         "equation"
                         "Insert a LaTeX for equation"
                         ;; 'yyuu-tempo-tags-tex
                         )

  (tempo-define-template "align"
                         '("\\begin{align*}" > n>
                           r> n>
                           "\\end{align*}" > n>)
                         "align"
                         "Insert a LaTeX align for math"
                         ;; 'yyuu-tempo-tags-tex
                         )

  (tempo-define-template "align-cases"
                         '("\\begin{align*}" > n>
                           "\\begin{cases}" > n>
                           r> n>
                           "\\end{cases}" > n>
                           "\\end{align*}" > n>)
                         "cases"
                         "Insert a LaTeX align and nested cases for math"
                         ;; 'yyuu-tempo-tags-tex
                         )

  (tempo-define-template "cite"
                         '("[cite:@"r"]" > n>)
                         "cite"
                         "Insert an Org cite citation."
                         ;; 'yyuu-tempo-tags-org
                         )

  (tempo-define-template "printb"
                         '("#+print_bibliography:" > n>)
                         "printb"
                         "Insert a printed bibliography Org command."
                         ;; 'yyuu-tempo-tags-org
                         )

  (tempo-define-template "homep"
                         '("home-manager.users.yuu = {" > n>
                           n
                           "home.packages = with pkgs; [" > n>
                           "];" > n>
                           n
                           "};" >)
                         "homep"
                         "Insert a Nix home-manager home.packages"
                         ;; 'yyuu-tempo-tags-nix
                         )

  (tempo-define-template "sysp"
                         '("environment.systemPackages = with pkgs; [" > n>
                           "];" >)
                         "sysp"
                         "Insert a Nix environment.systemPackages"
                         ;; 'yyuu-tempo-tags-nix
                         )

  ;; :hook
  ;; (tex-mode-hok . (lambda () (tempo-use-tag-list 'yyuu-tempo-tags-tex)))
  ;; (org-mode-hok . (lambda () (tempo-use-tag-list 'yyuu-tempo-tags-tex)))

  :bind
  (("C-;" . tempo-complete-tag)
   ("C-," . tempo-backward-mark)
   ("C-." . tempo-forward-mark)))


;;;; Footer

(provide 'yyuu-mod-emacs-uix-input)

;;; yyuu-mod-emacs-uix-input.el ends here
