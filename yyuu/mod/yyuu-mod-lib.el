;;; yyuu-mod-lib.el --- yyuu's standard library  -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Yuu Yin

;; Author: Yuu Yin <yuu@anche.no>
;; Keywords: extensions

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(defun yyuu-delete-from-list (list element)
  "Delete ELEMENT from LIST."
  (setq list (delete element list)))

(defun yyuu-screenshot-svg ()
  "Save a screenshot of the current frame as an SVG image.
Saves to a temp file and puts the filename in the kill ring.
Author: alphapapa, https://reddit.com/r/emacs/comments/idz35e/emacs_27_can_take_svg_screenshots_of_itself/g2c2c6y/?context=3"
  (interactive)
  (let* ((filename (make-temp-file "emacs-screenshot-" nil ".svg"))
         (data (x-export-frames nil 'svg)))
    (with-temp-file filename
      (insert data))
    (kill-new filename)
    (message filename)))


;;;; Datetime

(defun yyuu-datetime-iso-utc ()
  "Generate an ISO 8601 UTC datetime string."
  (concat
   (format-time-string "%Y-%m-%dT%T" nil "UTC0")
   ((lambda (x) (concat (substring x 0 3) ":" (substring x 3 5)))
    (format-time-string "%z" nil "UTC0"))))

(defun yyuu-datetime-utc ()
  "Generate an UTC datetime string."
  (format-time-string "%Y%m%d%H%M%S" nil "UTC0"))

(defun yyuu-datetime-utc-list ()
  "Generate list with ISO 8601 UTC and non-ISO UTC datetime string."
  (let* ((datetime-utc (format-time-string "%Y-%m-%dT%T" nil "UTC0"))
         (datetime-iso-utc (concat
                            datetime-utc
                            ((lambda (x) (concat (substring x 0 3) ":" (substring x 3 5)))
                             (format-time-string "%z" nil "UTC0")))))
    `(,datetime-iso-utc
      ,(replace-regexp-in-string "[-:T]" "" datetime-utc))))

(defun yyuu-insert-datetime-utc ()
  "Insert an ISO 8601 UTC datetime string."
  (interactive)
  (insert (yyuu-datetime-utc)))


;;;; GUI

;; Useful for e.g. setting fonts.
(defun yyuu-apply-if-gui (&rest action)
  "Do ACTION if GNU Emacs is in a GUI session, be it daemon or not."
  (if (daemonp)
      (add-hook 'after-make-frame-functions
                (lambda (frame)
                  (select-frame frame)
                  (if (display-graphic-p frame)
                      (apply action))))
    (if (display-graphic-p)
        (apply action))))


;;;; Footer

(provide 'yyuu-mod-lib)

;;; yyuu-mod-lib.el ends here
