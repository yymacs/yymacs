;;; yyuu-mod-pro-org-ys-yu.el --- ys yu: Yuu Yin's personal task management (PTM)  -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Yuu Yin

;; Author: Yuu Yin <yuu@anche.no>
;; Keywords: extensions

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:


;;;;; Definitions

(defcustom yyuu-mod-pro-org-ys-yu-directory
  (expand-file-name "yu/" yyuu-mod-pro-org-ys-directory)
  "ys `yu's (to dos) directory."
  :type '(directory)
  :group 'yyuu)

(defcustom yyuu-mod-pro-org-ys-yu-yunx-file-path
  (expand-file-name "yunx.org" yyuu-mod-pro-org-ys-yu-directory)
  "ys yu `yunx's (capture (GTD1), in, inbox) Org file path. Has tasks
in the first stage (GTD1) of GTD: capture. Then clarify (GTD2),
organize (GTD3) entries in `yunx', and then `org-refile' (e.g.
refile actionable/engageable (GTD5) entries to `yuy's `yd' or `ydd')."
  :type '(file :must-match t)
  :group 'yyuu)

(defcustom yyuu-mod-pro-org-ys-yu-yuy-file-path
  (expand-file-name "yuy.org" yyuu-mod-pro-org-ys-yu-directory)
  "ys yu yuy's Org file path. Has actionable/engageable (GTD5) tasks
with a single action (actionable tasks other than projects) under
`yd', and multiple actions (projects) under `ydd'."
  :type '(file :must-match t)
  :group 'yyuu)

;; (defcustom yyuu-mod-pro-org-ys-yu-yd-file-path
;;   (expand-file-name "yd.org" yyuu-mod-pro-org-ys-yu-directory)
;;   "ys yu ydd's Org file path. Has actionable/engageable (GTD5) tasks
;; with a single action, i.e. actionable tasks other than projects."
;;   :type '(file :must-match t)
;;   :group 'yyuu)

;; (defcustom yyuu-mod-pro-org-ys-yu-ydd-file-path
;;   (expand-file-name "ydd.org" yyuu-mod-pro-org-ys-yu-directory)
;;   "ys yu ydd's Org file path. Has actionable/engageable (GTD5) tasks
;; with multiple actions, i.e. projects."
;;   :type '(file :must-match t)
;;   :group 'yyuu)

(defcustom yyuu-mod-pro-org-ys-yu-yui-file-path
  (expand-file-name "yui.org" yyuu-mod-pro-org-ys-yu-directory)
  "ys yu yui's (ideas for projects, ...) file path. Yui, someday,
maybe..."
  :type '(file :must-match t)
  :group 'yyuu)

(defcustom yyuu-mod-pro-org-ys-yu-yux-file-path
  (expand-file-name "yux.org" yyuu-mod-pro-org-ys-yu-directory)
  "ys yu yux's file path. Unactionable tasks."
  :type '(file :must-match t)
  :group 'yyuu)


;;;;; Setup: capture, refile

(use-package org
  :custom
  (org-default-notes-file yyuu-mod-pro-org-ys-yu-yunx-file-path)

  ;; Enable refile as level 1 heading instead of default only as subheadings.
  ;; Ref.: https://stackoverflow.com/questions/21334817
  (org-refile-use-outline-path 'file)
  (org-outline-path-complete-in-steps nil)
  (org-reverse-note-order t)

  (org-refile-targets `((,yyuu-mod-pro-org-ys-yu-yunx-file-path . (:maxlevel . 1))
                        (,yyuu-mod-pro-org-ys-yu-yuy-file-path . (:maxlevel . 1))
                        (,yyuu-mod-pro-org-ys-yu-yui-file-path . (:maxlevel . 1))
                        (,yyuu-mod-pro-org-ys-yu-yux-file-path . (:maxlevel . 1))))


  :config
  (add-to-list
   'org-capture-templates
   `("yun" "yunx" entry (file ,yyuu-mod-pro-org-ys-yu-yunx-file-path)
      "* PROCESS %^{heading}
:LOGBOOK:
- State \"PROCESS\"       from              %U
:END:

%?
"
      :prepend t
      :empty-lines 1)))


(provide 'yyuu-mod-pro-org-ys-yu)
;;; yyuu-mod-pro-org-ys-yu.el ends here
