;;; yyuu-mod-pro-org-ys.el --- ys: Yuu Yin's personals            -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Yuu Yin

;; Author: Yuu Yin <yuu@anche.no>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:


;;;;Definitions

(defcustom yyuu-mod-pro-org-ys-directory
  (expand-file-name "ys/" org-directory)
  "`ys's directory."
  :type '(directory)
  :group 'yyuu)


;;;; Capture

(use-package org
  :config
  (add-to-list 'org-capture-templates '("y" "ys"))
  (add-to-list 'org-capture-templates '("yi" "yi"))
  (add-to-list 'org-capture-templates '("yu" "yu"))
  (add-to-list 'org-capture-templates '("ya" "ya")))


;;;; Requires-post

(require 'yyuu-mod-pro-org-ys-yi)
(require 'yyuu-mod-pro-org-ys-yu)
(require 'yyuu-mod-pro-org-ys-ya)


;; Footer

(provide 'yyuu-mod-pro-org-ys)

;;; yyuu-mod-pro-org-ys.el ends here
