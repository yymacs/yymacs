;;; yyuu-mod-comp-lang-c.el --- C, C++               -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Yuu Yin

;; Author: Yuu Yin <yuu@anche.no>
;; Keywords: extensions

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:


;;;; Tree-sitter

(use-package c-ts-mode
  :config
  (add-to-list 'major-mode-remap-alist '(c-mode . c-ts-mode))
  (add-to-list 'auto-mode-alist '("\\.c\\'" . c-ts-mode))

  (add-to-list 'major-mode-remap-alist '(c++-mode . c++-ts-mode))
  (add-to-list 'auto-mode-alist '("\\.cpp\\'" . c++-ts-mode)))


;;;; Language Server Protocol (LSP)

(use-package eglot
  :hook
  (c-ts-mode . eglot-ensure)
  (c++-ts-mode . eglot-ensure))


;;;; Footer

(provide 'yyuu-mod-comp-lang-c)

;;; yyuu-mod-comp-lang-c.el ends here
