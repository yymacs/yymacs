;;; yyuu-mod-comp-lang-javascript.el --- JavaScript, TypeScript  -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Yuu Yin

;; Author: Yuu Yin <yuu@anche.no>
;; Keywords: extensions

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:


;;;; Tree-sitter

(use-package treesit
  :config
  (add-to-list 'major-mode-remap-alist '(js-mode . js-ts-mode))
  (add-to-list 'auto-mode-alist '("\\.js[x]?\\'" . js-ts-mode))
  (add-to-list 'auto-mode-alist '("\\.ts[x]?\\'" . tsx-ts-mode)))


;;;; Language Server Protocol (LSP)

(use-package eglot
  :hook
  (js-ts-mode . eglot-ensure)
  (js-mode . eglot-ensure)
  (typescript-ts-mode . eglot-ensure)
  (tsx-ts-mode . eglot-ensure))


;;;; TypeScript

(use-package typescript-ts-mode
  :hook
  (typescript-ts-mode . (lambda () (setq-local compile-command "tsc "))))


;;;; Flymake

(use-package flymake
  :hook
  (js-base-mode . flymake-mode)
  (js-mode . flymake-mode)
  (js-ts-mode . flymake-mode))


;;;; Org-babel

(use-package org
  :config
  (add-to-list 'org-babel-load-languages '(js . t))
  (org-babel-do-load-languages 'org-babel-load-languages org-babel-load-languages))

(use-package ob-tangle
  :config
  (add-to-list 'org-babel-tangle-lang-exts '("js" . "js")))


;;; Footer

(provide 'yyuu-mod-comp-lang-javascript)

;;; yyuu-mod-comp-lang-javascript.el ends here
