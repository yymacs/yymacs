;;; yyuu-mod-comp-lang-misc.el --- miscellaneous computer languages  -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Yuu Yin

;; Author: Yuu Yin <yuu@anche.no>
;; Keywords: extensions

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:


;;;; Markup languages


;;;;; Markdown

(use-package markdown-mode)


;;;;; Mediawiki

(use-package mediawiki
  :mode ("\\.mw\\'" . mediawiki-mode))


;;;;; HTML

(use-package html-ts-mode
  :config
  (add-to-list 'major-mode-remap-alist '(mhtml-mode . html-ts-mode)))


;;;;; CSS, SCSS

;; (use-package css-ts-mode
;;   :config
;;   (add-to-list 'major-mode-remap-alist '(css-mode . css-ts-mode))
;;   (add-to-list 'major-mode-remap-alist '(css-base-mode . css-ts-mode)))

(use-package ssass-mode
  :defer t

  :mode ("\\.scss\\'" . ssass-mode))


;;;; Data interchange/exchange


;;;;; JSON


;;;;;; Tree-sitter

(use-package json-ts-mode
  :config
  (add-to-list 'major-mode-remap-alist '(js-json-mode . json-ts-mode))
  (add-to-list 'auto-mode-alist '("\\.json\\'" . json-ts-mode))

  :hook
  (json-ts-mode . flymake-mode))


;;;;;; Language Server Protocol (LSP)

(use-package eglot
  :hook
  (json-mode . eglot-ensure)
  (json-ts-mode . eglot-ensure))


;;;;; TOML

(use-package toml-ts-mode
  :config
  (add-to-list 'major-mode-remap-alist '(conf-toml-mode . toml-ts-mode))
  (add-to-list 'auto-mode-alist '("\\.toml\\'" . toml-ts-mode)))


;;;;; YAML

(use-package yaml-ts-mode
  :config
  (add-to-list 'auto-mode-alist '("\\.ya?ml\\'" . yaml-ts-mode)))


;;;;; CSV

(use-package csv-mode
  :defer t)


;;;; Metalanguage

(use-package bnf-mode
  :defer t)

(use-package ebnf-mode
  :defer t)


;;;; Polymode

;; (use-package polymode)

;; (use-package poly-org
;;   :defer t
;;   :after (org)

;;   :hook
;;   (org-mode . poly-org-mode))

;; (use-package poly-R
;;   :defer t)


;;;; Exercism

(use-package exercism
  :bind
  ("C-c d x t" . exercism-set-track)
  ("C-c d x e" . exercism-open-exercise)
  ("C-c d x E" . exercism-open-exercise-offline)
  ("C-c d x c" . exercism-run-tests)
  ("C-c d x s" . exercism-submit)
  ("C-c d x S" . exercism-submit-then-open-in-browser))


;;; Footer

(provide 'yyuu-mod-comp-lang-misc)

;;; yyuu-mod-comp-lang-misc.el ends here
