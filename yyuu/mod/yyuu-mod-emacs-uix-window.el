;;; yyuu-mod-emacs-uix-window.el --- yyuu's UI window                 -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Yuu Yin

;; Author: Yuu Yin <yuu@anche.no>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:


;;;; Appearance

(use-package window
  :bind
  ("C-c w [" . enlarge-window)
  ("C-c w ]" . shrink-window)
  ("C-c w {" . enlarge-window-horizontally)
  ("C-c w }" . shrink-window-horizontally))

;; ;; TODO: window-divider
;; (use-package frame
;;   :custom
;;   (window-divider)

;;   :hook
;;   (after-init . window-divider-mode))


;;;; Navigation

;; For navigating window in directions.
(use-package windmove
  :bind (("C-S-h" . windmove-left)
         ("C-c w h" . windmove-left)
         ("C-S-l" . windmove-right)
         ("C-c w l" . windmove-right)
         ("C-S-k" . windmove-up)
         ("C-c w k" . windmove-up)
         ("C-S-j" . windmove-down)
         ("C-c w j" . windmove-down)
         ("C-c w H" . windmove-swap-states-left)
         ("C-c w L" . windmove-swap-states-right)
         ("C-c w K" . windmove-swap-states-up)
         ("C-c w J" . windmove-swap-states-down)))

;; For navigating window by number label.
(use-package winum
  :custom
  ;; Count windows locally, not globally, so for fast switching in each frame
  ;; with key numbers.
  (winum-scope 'frame-local)

  :config
  (winum-mode)

  :bind (("C-c w 1" . winum-select-window-1)
         ("M-1" . winum-select-window-1)
         ("C-c w 2" . winum-select-window-2)
         ("M-2" . winum-select-window-2)
         ("C-c w 3" . winum-select-window-3)
         ("M-3" . winum-select-window-3)
         ("C-c w 4" . winum-select-window-4)
         ("M-4" . winum-select-window-4)
         ("C-c w 5" . winum-select-window-5)
         ("M-5" . winum-select-window-5)
         ("C-c w 6" . winum-select-window-6)
         ("M-6" . winum-select-window-6)
         ("C-c w 7" . winum-select-window-7)
         ("M-7" . winum-select-window-7)
         ("C-c w 8" . winum-select-window-8)
         ("M-8" . winum-select-window-8)
         ("C-c w 9" . winum-select-window-9)
         ("M-9" . winum-select-window-9)))


(provide 'yyuu-mod-emacs-uix-window)
;;; yyuu-mod-emacs-uix-window.el ends here
