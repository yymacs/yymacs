;;; yyuu-mod-comp-lang-nix.el --- Nix                              -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Yuu Yin

;; Author: Yuu Yin <yuu@anche.no>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;


;;; Code:

(use-package nix-mode
  :custom
  (nix-executable (file-truename (executable-find "nix"))))

(use-package nix3
  :defer t)


;;;; Language Server Protocol (LSP)

(use-package eglot
  :config
  (add-to-list 'eglot-server-programs `(nix-mode . (,(file-truename (executable-find "nil")))))
  :hook
  (nix-mode . eglot-ensure))


;;;; Tree-Sitter

;; (use-package treesit
;;   :config
;;   (add-to-list 'major-mode-remap-alist '(nix-mode . nix-ts-mode)))


;;;; Footer

(provide 'yyuu-mod-comp-lang-nix)

;;; yyuu-mod-comp-lang-nix.el ends here
