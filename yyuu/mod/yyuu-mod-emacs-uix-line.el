;;; yyuu-mod-emacs-uix-line.el --- yyuu's user interface line   -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Yuu Yin

;; Author: Yuu Yin <yuu@anche.no>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;


;;; Code:

;; (require 'simple)    ; visual-line-mode
;; (require 'info)      ; Info-mode-hook
;; (require 'man)       ; man-common-hook
;; (require 'prog-mode) ; prog-mode-hook
;; (require 'text-mode) ; text-mode-hook

(keymap-global-set "M-[" #'join-line)

;; Continue wrapped words at whitespace, rather than in the middle of a word.
(setq-default word-wrap t)
;; ...but do not do any wrapping by default becasue it is expensive. (Enable
;; `visual-line-mode' for soft line-wrapping, OR `auto-fill-mode' for hard
;; line-wrapping).
(setq-default truncate-lines t)
;; IF enabled (AND `truncate-lines' was disabled), THEN soft wrapping no longer
;; occurs WHEN that window is less than `truncate-partial-width-windows'
;; characters wide. We do not need this, and it is extra work for Emacs
;; otherwise, so off it goes.
(setq truncate-partial-width-windows nil)

;; This was a widespread practice in the days of typewriters. Nowadays, for
;; writing prose with monospace fonts, but it is obsolete otherwise.
(customize-set-variable 'sentence-end-double-space nil)

;; The POSIX standard defines a line is "a sequence of zero or more
;; non-newline characters followed by a terminating newline", so files
;; should end in a newline. Windows doesn't respect this (because it's
;; Windows), but we should, since programmers' tools tend to be POSIX
;; compliant (and no big deal if not).
(customize-set-variable 'require-final-newline t)

;; Default to soft line-wrapping in general textual modes. It is more
;; sensibile for text modes, even if hard wrapping is more performant.
;; (add-hook 'special-mode-hook #'visual-line-mode)
(add-hook 'Info-mode-hook #'visual-line-mode)  ; special-mode
(add-hook 'man-common-hook #'visual-line-mode) ; special-mode
(add-hook 'conf-mode-hook #'visual-line-mode)
(add-hook 'prog-mode-hook #'visual-line-mode)
(add-hook 'text-mode-hook #'visual-line-mode)

;; Enable line numbers by default only for programming, as to avoid
;; distraction in other modes.
(add-hook 'prog-mode-hook #'(lambda () (display-line-numbers-mode 'relative)))
(add-hook 'sgml-mode-hook #'(lambda () (display-line-numbers-mode 'relative)))

(customize-set-variable 'display-line-numbers-type 'relative)


(provide 'yyuu-mod-emacs-uix-line)
;;; yyuu-mod-emacs-uix-line.el ends here
