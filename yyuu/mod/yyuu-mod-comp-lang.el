;;; yyuu-mod-comp-lang.el --- computer languages     -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Yuu Yin

;; Author: Yuu Yin <yuu@anche.no>
;; Keywords: extensions

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:


;;;; Language Server Protocol (LSP)

(use-package eglot
  :bind
  ("C-c d e" . eglot)
  ("C-c d r" . eglot-rename)
  ("C-c d a" . eglot-code-actions)
  ("C-c d S" . eglot-shutdown)
  ("C-c d K" . eglot-shutdown-all)
  ("C-c d f b" . eglot-format-buffer)
  ("C-c d f f" . eglot-format)
  ("C-c d f r" . eglot-format)
  ("C-c d f i" . eglot-code-action-organize-import)
  ("C-c d m" . eglot-menu))


;;;; Tree-sitter

(use-package treesit
  :defer t)

;; Automatic usage and fallback for tree-sitter major modes.
;; Needed because Emacs 29 treesit has not that by default.
;; (use-package treesit-auto
;;   :custom
;;   (treesit-auto-install nil)

;;   :config
;;   (global-treesit-auto-mode))


;;;; Debugging: Debug Adapter Protocol

(use-package dape)


;;;; Syntax checker, linter: Flymake

(use-package flymake
  :hook
  (prog-mode . flymake-mode)
  (sgml-mode . flymake-mode)

  :bind
  ("C-c d c C" . flymake-mode)
  ("C-c d c c" . flymake-start)
  ("C-c d c m" . flymake-menu)
  ("C-c d p" . flymake-goto-prev-error)
  ("C-c d c p" . flymake-goto-prev-error)
  ("C-c d n" . flymake-goto-next-error)
  ("C-c d c n" . flymake-goto-next-error))


;;;; Documentation, references

(use-package xref
  :bind
  ("C-c d d" . xref-find-definitions)
  ("C-c d D" . xref-find-references))

(use-package devdocs
  :defer t

  :hook
  (devdocs-mode . visual-line-mode)

  :bind
  ("C-c d h i" . devdocs-install)
  ("C-c d h h" . devdocs-lookup)
  ("C-c d h D" . devdocs-delete)
  ("C-c d h U" . devdocs-update-all))


(provide 'yyuu-mod-comp-lang)
;;; yyuu-mod-comp-lang.el ends here
