;;; yyuu-mod-emacs-uix-completion.el --- yyuu's user interface completion  -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Yuu Yin

;; Author: Yuu Yin <yuu@anche.no>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;


;;; Code:


;;;; Completion backend, engines, algorithms.

;; Orderless completion. Using this instead of fussy and flx-rs because seems
;; less buggy.
(use-package orderless
  :ensure t
  ;; :custom
  ;; (completion-styles '(orderless basic))
  ;; (completion-category-overrides '((file (styles basic partial-completion))))
  )

;; Fuzzy completion.
;; FIXME: `fussy' directory completion is sometimes buggy, needing duplicated
;; entries for showing next.
(use-package fussy
  :ensure t

  :config
  (push 'fussy completion-styles)

  :custom
  ;; For example, project-find-file uses `project-files' which uses substring
  ;; completion by default. Set to nil to make sure it is using flx.
  (completion-category-defaults nil)
  (completion-category-overrides nil)

  (fussy-use-cache nil)

  ;; (fussy-filter-fn 'fussy-filter-default)
  (fussy-filter-fn 'fussy-filter-orderless-flex))

(use-package fussy
  :after
  (eglot)

  :config
  ;; Use `flx-rs' for `eglot' instead of default `flex'.
  (add-to-list 'completion-category-overrides
                 '(eglot (styles fussy basic))))

;; FIXME: `flx-rs' blocks GNU Emacs, while increasing memory consumption,
;; eventually crashing GNU Emacs.
;; (use-package flx-rs
;;   :requires
;;   (fussy)

;;   :after
;;   (fussy)

;;   :custom
;;   (fussy-score-fn 'flx-rs-score)

;;   :config
;;   (flx-rs-load-dyn)

;;   ;; Completely replace `flx' with `flx-rs'.
;;   (advice-add 'flx-score :override #'flx-rs-score))

;; Finding Files and URLs at Point. Substitutes find-file
(use-package ffap
  :hook
  (after-init . ffap-bindings))


;;;; In-buffer completion

(use-package corfu
  :init
  (global-corfu-mode)

  :custom
  (corfu-auto t)
  (corfu-auto-delay 0.1)
  (corfu-popupinfo-delay '(2.0 . 1.0))
  (corfu-auto-prefix 2)

  :hook
  (corfu-mode . corfu-popupinfo-mode)

  :bind
  ("C-<tab>" . corfu-complete))

;; FIXME: `company-files--grab-existing-name' void function regexp
;; (use-package company
;;   :hook
;;   (after-init . global-company-mode)

;;   :bind
;;   ("C-<tab>" . company-complete))


;;;; TODO: Implement what is useful from `9.4.5 Completion Options'.


;;;; Footer

(provide 'yyuu-mod-emacs-uix-completion)

;;; yyuu-mod-emacs-uix-completion.el ends here
