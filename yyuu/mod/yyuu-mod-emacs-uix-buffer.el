;;; yyuu-mod-emacs-uix-buffer.el --- yyuu's user interface buffer  -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Yuu Yin

;; Author: Yuu Yin <yuu@anche.no>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;


;;; Code:


;;;; Keymap

(keymap-global-set "C-x P" #'previous-buffer)
(keymap-global-set "C-x N" #'next-buffer)


;;;; IBuffer
(use-package ibuffer
  :bind
  ("C-x C-b" . ibuffer)
  ("C-x C-S-b" . switch-to-buffer))


;;;; Minibuffer

(use-package minibuffer
  :custom
  ;; A minibuffer will appear only in the frame which created it.
  ;; Cumbersome to have same minibuffer for all frames.
  (minibuffer-follows-selected-frame nil)

  :bind
  (:map minibuffer-mode-map
        ;; FIXME: `pixel-scroll-interpolate' (see the `uix' module) does not work
        ;; on `minibuffer-mode'. So set `scroll-command' for `minibuffer-mode'
        ;; instead.
        ("C-v" . scroll-up-command)
        ("M-v" . scroll-down-command)))


;;;;; FIDO

;; Bad performance (e.g. when thousands entries), so I disabled it.
;; (use-package fido
;;   :custom
;;   (fido-mode . nil)
;;   (fido-vertical-mode . nil)
;;   :hook
;;   (icomplete-vertical-mode . visual-line-mode)
;;   (fido-vertical-mode . visual-line-mode))


;;;;; Vertico

(use-package vertico
  :init
  (vertico-mode)
  :bind
  (:map vertico-map ("C-<return>" . vertico-exit-input)))

;; Persist history over Emacs restarts. Vertico sorts by history position.
(use-package savehist
  :init
  (savehist-mode))

;; A few more useful configurations...
(use-package emacs
  :init
  ;; Add prompt indicator to `completing-read-multiple'.
  ;; We display [CRM<separator>], e.g., [CRM,] if the separator is a comma.
  (defun crm-indicator (args)
    (cons (format "[CRM%s] %s"
                  (replace-regexp-in-string
                   "\\`\\[.*?]\\*\\|\\[.*?]\\*\\'" ""
                   crm-separator)
                  (car args))
          (cdr args)))
  (advice-add #'completing-read-multiple :filter-args #'crm-indicator)

  ;; Do not allow the cursor in the minibuffer prompt
  (setq minibuffer-prompt-properties
        '(read-only t cursor-intangible t face minibuffer-prompt))
  (add-hook 'minibuffer-setup-hook #'cursor-intangible-mode)

  ;; Emacs 28: Hide commands in M-x which do not work in the current mode.
  ;; Vertico commands are hidden in normal buffers.
  ;; (setq read-extended-command-predicate
  ;;       #'command-completion-default-include-p)

  ;; Enable recursive minibuffers
  (setq enable-recursive-minibuffers t)

  ;; Case insensitive
  (setq read-file-name-completion-ignore-case t
        read-buffer-completion-ignore-case t
        completion-ignore-case t))


;;;;; Margin


(use-package marginalia
  ;; Either bind `marginalia-cycle' globally or only in the minibuffer
  :bind (("M-A" . marginalia-cycle)
         :map minibuffer-local-map
         ("M-A" . marginalia-cycle))

  ;; The :init configuration is always executed (not lazy!)
  :init

  ;; Must be in the :init section of use-package such that the mode gets enabled
  ;; right away. Note that this forces loading the package.
  (marginalia-mode))


(provide 'yyuu-mod-emacs-uix-buffer)
;;; yyuu-mod-emacs-uix-buffer.el ends here
