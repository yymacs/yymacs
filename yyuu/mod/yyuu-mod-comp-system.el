;;; yyuu-mod-comp-system.el --- yyuu's system                 -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Yuu Yin

;; Author: Yuu Yin <yuu@anche.no>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;


;;; Code:


;;;; Shell


;;;;; POSIX

;; Ensure POSIX shell in case user's default shell is non-POSIX
;; (nushell, fish, ...) because Emacs and Elisp packages expect POSIX shell.
;; Ref: https://stackoverflow.com/a/37523213
(setq-default explicit-shell-file-name "/bin/sh") ; shell
(setq-default shell-file-name "/bin/sh")
(setq-default sh-shell "sh") ; sh-script


;;;;; Eshell

;; Customize Eshell prompt.
(use-package em-prompt
  :custom
  (eshell-prompt-function (lambda ()
                            (concat
                             (if (= (file-user-uid) 0) "#" "λ")
                             " "
                             (getenv "USER")
                             " @ "
                             system-name
                             " "
                             (abbreviate-file-name (eshell/pwd))
                             " "
                             (format-time-string "%FT%T")
                             "\n"))))

;;;;; ESHELL + EAT

(use-package eat
  :hook
  (eshell-mode . eat-eshell-mode))


;;;; Nix/NixOS

(use-package tramp
  :config
  (dolist (yyuu-mod-nix-path '("/etc/nixos/bin"
                               "/nix/var/nix/profiles/default/bin"
                               "/run/current-system/sw/bin"
                               "/run/wrappers/bin"
                               "~/.nix-profile/bin"))
    (add-to-list 'tramp-remote-path yyuu-mod-nix-path)))


;;;; GNU Guix System

;; /ssh:root@192.168.122.59#22:/mnt
;; TRAMP File error: Couldn't find a proper `ls' command
;; https://lists.gnu.org/archive/html/help-guix/2016-10/msg00046.html
;; https://logs.guix.gnu.org/guix/2022-06-22.log#001124
;; Make sure we work on remote  machines :)
;; probably only helps if you start on a guixsd machine..!
(use-package tramp
  :config
  (dolist (yyuu-mod-guix-path `(,(file-truename  "~/.guix-profile/bin")
                                ,(file-truename "~/.guix-profile/sbin")
                                "/run/setuid-programs"
                                "/run/current-system/profile/bin"
                                "/run/current-system/profile/sbin"))
    (add-to-list 'tramp-remote-path yyuu-mod-guix-path)))


;;; Footer

(provide 'yyuu-mod-comp-system)

;;; yyuu-mod-comp-system.el ends here
