;;; yyuu-mod-emacs-uix-column.el --- yyuu's user interface column  -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Yuu Yin

;; Author: Yuu Yin <yuu@anche.no>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;


;;; Code:

;; 80 characters per line is the most popular for programming, also acceptable
;; for 480px screens, also for zooming in 1080px screens.
(setq-default fill-column 80)


(provide 'yyuu-mod-emacs-uix-column)
;;; yyuu-mod-emacs-uix-column.el ends here
