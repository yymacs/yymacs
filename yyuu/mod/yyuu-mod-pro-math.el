;;; yyuu-mod-pro-math.el --- Mathematics             -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Yuu Yin

;; Author: Yuu Yin <yuu@anche.no>
;; Keywords: extensions

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(use-package wolfram
  :defer t
  :custom
  (wolfram-alpha-query-history 10)

  :bind
  ("C-c m w" . wolfram-alpha))

(use-package calc
  :defer t
  :bind
  ("C-c m g" . calc-grab-region)
  ("C-c m y" . calc-copy-to-buffer)
  ("C-c m Y" . calc-yank))


(provide 'yyuu-mod-pro-math)
;;; yyuu-mod-pro-math.el ends here
