;;; yyuu-mod-pro-doc.el --- Document formats, databases  -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Yuu Yin

;; Author: Yuu Yin <yuu@anche.no>
;; Keywords: extensions

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:


;;;; EPUB

(use-package nov
  :mode
  ("\\.epub\\'" . nov-mode)

  :config
  (defun yyuu-nov-mode-setup ()
    (face-remap-add-relative 'variable-pitch
                             :family "Open Sans"
                             :height 1.8
                             :width 'semi-expanded)
    (face-remap-add-relative 'fixed-pitch
                             :family "Iosevka Fixed"
                             :width 'regular
                             :weight 'normal
                             :slant 'normal
                             :height 1.8)
    (face-remap-add-relative 'default :height 1.0)
    (setq-local line-spacing 0.2
                next-screen-context-lines 4
                shr-use-colors nil)
    (setq-local visual-fill-column-center-text t
                visual-fill-column-width 80
                nov-text-width nil)
    (hl-line-mode -1)
    (visual-line-mode))

  :hook
  (nov-mode . yyuu-nov-mode-setup))


;;;; PDF

(use-package pdf-view
  :bind
  (:map pdf-view-mode-map
        ;; FIXME: `pixel-scroll-interpolate' (see the `uix' module) does not work
        ;; on `pdf-view-mode'. So set `scroll-command' for `pdf-view-mode'
        ;; instead.
        ("C-v" . scroll-up-command)
        ("M-v" . scroll-down-command)))

(use-package pdf-tools
  :preface
  (add-to-list 'display-buffer-alist
               '("^\\*eldoc for" display-buffer-at-bottom
                 (window-height . 4)))

  :mode
  ("\\.pdf\\'" . pdf-view-mode)

  :bind
  (:map pdf-view-mode-map
        ("o" . pdf-outline))

  :defer t
  :hook
  ;; (pdf-outline-minor-mode . (lambda () (window-resize nil delta 2 t)))
  ;; (pdf-outline-minor-mode . (lambda () (window-resize nil (/ (frame-width) 20) 2 t)))
  ;; (pdf-outline-buffer-mode . (window-width (- (/ (window-width) 6))))
  ;; (pdf-view-mode . pdf-view-midnight-minor-mode)
  (pdf-view-mode . pdf-isearch-minor-mode)
  (outline-mode . (lambda () (setq-local display-line-numbers-mode -1)))
  (outline-mode . (lambda () (setq-local truncate-lines t))))


;;;; Calibre

(use-package calibredb
  :demand t
  :init
  (autoload 'calibredb "calibredb")

  :config
  (setq calibredb-root-dir "/mnt/media/doc/calibre/")
  (setq calibredb-db-dir (expand-file-name "metadata.db" calibredb-root-dir))

  (defvar calibredb-show-mode-map
    (let ((map (make-sparse-keymap)))
      (define-key map "?" #'calibredb-entry-dispatch)
      (define-key map "o" #'calibredb-find-file)
      (define-key map "O" #'calibredb-find-file-other-frame)
      (define-key map "V" #'calibredb-open-file-with-default-tool)
      (define-key map "s" #'calibredb-set-metadata-dispatch)
      (define-key map "e" #'calibredb-export-dispatch)
      (define-key map "q" #'calibredb-entry-quit)
      (define-key map "y" #'calibredb-yank-dispatch)
      (define-key map "," #'calibredb-quick-look)
      (define-key map "." #'calibredb-open-dired)
      (define-key map "\M-/" #'calibredb-rga)
      (define-key map "\M-t" #'calibredb-set-metadata--tags)
      (define-key map "\M-a" #'calibredb-set-metadata--author_sort)
      (define-key map "\M-A" #'calibredb-set-metadata--authors)
      (define-key map "\M-T" #'calibredb-set-metadata--title)
      (define-key map "\M-c" #'calibredb-set-metadata--comments)
      map)
    "Keymap for `calibredb-show-mode'.")

  (defvar calibredb-search-mode-map
    (let ((map (make-sparse-keymap)))
      (define-key map [mouse-3] #'calibredb-search-mouse)
      (define-key map (kbd "<RET>") #'calibredb-find-file)
      (define-key map "?" #'calibredb-dispatch)
      (define-key map "a" #'calibredb-add)
      (define-key map "A" #'calibredb-add-dir)
      (define-key map "c" #'calibredb-clone)
      (define-key map "d" #'calibredb-remove)
      (define-key map "D" #'calibredb-remove-marked-items)
      (define-key map "j" #'calibredb-next-entry)
      (define-key map "k" #'calibredb-previous-entry)
      (define-key map "l" #'calibredb-virtual-library-list)
      (define-key map "L" #'calibredb-library-list)
      (define-key map "n" #'calibredb-virtual-library-next)
      (define-key map "N" #'calibredb-library-next)
      (define-key map "p" #'calibredb-virtual-library-previous)
      (define-key map "P" #'calibredb-library-previous)
      (define-key map "s" #'calibredb-set-metadata-dispatch)
      (define-key map "S" #'calibredb-switch-library)
      (define-key map "o" #'calibredb-find-file)
      (define-key map "O" #'calibredb-find-file-other-frame)
      (define-key map "v" #'calibredb-view)
      (define-key map "V" #'calibredb-open-file-with-default-tool)
      (define-key map "," #'calibredb-quick-look)
      (define-key map "." #'calibredb-open-dired)
      (define-key map "y" #'calibredb-yank-dispatch)
      (define-key map "b" #'calibredb-catalog-bib-dispatch)
      (define-key map "e" #'calibredb-export-dispatch)
      (define-key map "r" #'calibredb-search-refresh-and-clear-filter)
      (define-key map "R" #'calibredb-search-clear-filter)
      (define-key map "q" #'calibredb-search-quit)
      (define-key map "m" #'calibredb-mark-and-forward)
      (define-key map "f" #'calibredb-toggle-favorite-at-point)
      (define-key map "x" #'calibredb-toggle-archive-at-point)
      (define-key map "h" #'calibredb-toggle-highlight-at-point)
      (define-key map "u" #'calibredb-unmark-and-forward)
      (define-key map "i" #'calibredb-edit-annotation)
      (define-key map (kbd "<DEL>") #'calibredb-unmark-and-backward)
      (define-key map (kbd "<backtab>") #'calibredb-toggle-view)
      (define-key map (kbd "TAB") #'calibredb-toggle-view-at-point)
      (define-key map "\M-n" #'calibredb-show-next-entry)
      (define-key map "\M-p" #'calibredb-show-previous-entry)
      (define-key map "/" #'calibredb-search-live-filter)
      (define-key map "\M-t" #'calibredb-set-metadata--tags)
      (define-key map "\M-a" #'calibredb-set-metadata--author_sort)
      (define-key map "\M-A" #'calibredb-set-metadata--authors)
      (define-key map "\M-T" #'calibredb-set-metadata--title)
      (define-key map "\M-c" #'calibredb-set-metadata--comments)
      map)
    "Keymap for `calibredb-search-mode'.")

  :bind
  ("C-c o b" . calibredb))


(provide 'yyuu-mod-pro-doc)
;;; yyuu-mod-pro-doc.el ends here
