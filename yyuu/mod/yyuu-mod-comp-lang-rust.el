;;; yyuu-mod-comp-lang-rust.el --- Rust                     -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Yuu Yin

;; Author: Yuu Yin <yuu@anche.no>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;


;;; Code:


;;;; Tree-sitter

(use-package rust-ts-mode
  :demand t)

(use-package toml-ts-mode
  :config
  (add-to-list 'auto-mode-alist '("Cargo.lock\\'" . toml-ts-mode)))


;;;; Major mode

(use-package rustic
  :demand t

  :init
  (setq rustic-treesitter-derive t)

  :config
  (add-to-list 'major-mode-remap-alist '(rust-mode . rustic-mode))

  :custom
  (rustic-lsp-server 'rust-analyzer)
  (rustic-analyzer-command `(,(executable-find "rust-analyzer")))

  (rustic-lsp-client 'eglot)

  (rustic-enable-detached-file-support t)

  (rustic-cargo-bin (file-truename
                     (executable-find "cargo")))

  (rustic-racer-rust-src-path (expand-file-name "lib/rustlib/rustc-src/rust/compiler/"
                                                (file-truename
                                                 (executable-find "rustc"))))
  :defer t
  :hook
  (eglot--managed-mode . (lambda () (flymake-mode -1)))

  :bind
  ("C-c d R a" . rustic-cargo-add)
  ("C-c d R b" . rustic-cargo-build)
  ("C-c d R c" . rustic-cargo-check)
  ("C-c d R n" . rustic-cargo-new)
  ("C-c d R r" . rustic-cargo-run)
  (:map rust-ts-mode-map
        ("C-c c a" . rustic-cargo-add)
        ("C-c c b" . rustic-cargo-build)
        ("C-c c c" . rustic-cargo-check)
        ("C-c c r" . rustic-cargo-run)))

(use-package rustic-babel
  :custom
  (rustic-babel-auto-wrap-main t)
  (rustic-babel-default-toolchain nil)
  (rustic-babel-cargo-params-commands-options '("run" "--quiet")))


;;;; Language Server Protocol (LSP)

(use-package eglot
  :hook
  (rustic-mode . eglot-ensure)
  (rust-ts-mode . eglot-ensure)
  (rust-mode . eglot-ensure))


;;;; Org Babel

(use-package rustic-babel
  :after (org)
  :defer t)


;;;; Footer

(provide 'yyuu-mod-comp-lang-rust)

;;; yyuu-mod-comp-lang-rust.el ends here
