;;; yyuu-mod-emacs-uix-space.el --- yyuu's user interface space          -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Yuu Yin

;; Author: Yuu Yin <yuu@anche.no>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;


;;; Code:


;;;; tab-bar

;; For creating [work]spaces using tab-bar.
;; (use-package tabspaces
;;   :hook
;;   (after-init . tabspaces-mode)

;;   :custom
;;   (setq tabspaces-use-filtered-buffers-as-default t)
;;   (setq tabspaces-default-tab "default")
;;   (setq tabspaces-remove-to-default t)
;;   (setq tabspaces-include-buffers '("*scratch*")))


;;;; Perject

;; The most holist/featurefull spaces management package so far.
(use-package perject
  :hook
  (after-init . perject-mode)

  :config
  (perject-mode 1)

  :custom
  (perject-valid-naming-chars '(? ?- ?. ?_))

  :bind
  ("C-x 5 2" . perject-create-new-frame)
  ("C-c s s" . perject-switch)            ; 2. create project
  ("C-c s o" . perject-open) ; 1. create colection
  ("C-c s c o" . perject-open) ; 1. create colection
  ("C-c s c c" . perject-open) ; 1. create colection
  ("C-c s o" . perject-open) ; 1. create colection
  ("C-c s a" . perject-save) ; 1. create colection
  ("C-c s c s" . perject-save)
  ("C-c s D" . perject-delete)
  ("C-c s r" . perject-rename)
  ("C-c s c C" . perject-close-collection)
  ("C-c s c r" . perject-reload-collection)
  ("C-c s c d" . perject-delete-collection)
  ("C-c s p d" . perject-delete-project)
  ("C-c s p n" . perject-next-project)
  ("C-c s p p" . perject-previous-project)
  ("C-c s c n" . perject-next-collection)
  ("C-c s c p" . perject-previous-collection)
  ("C-c s f c" . perject-create-new-frame)
  ("C-c s f 2" . perject-create-new-frame)
  ("C-c s b a" . perject-add-buffer-to-project) ; 3. add buffer to project
  ("C-c s b r" . perject-remove-buffer-from-project)
  ("C-c s b p" . perject-print-buffer-projects))

(use-package perject-transient
  :after
  (perject transient)

  :custom
  (perject-close-default '(t t t))

  :bind
  ("C-c s C" . perject-close))

;; (use-package perject-tab
;;   :after perject

;;   :init
;;   (perject-tab-mode 1)

;;   :bind
;;   (:map perject-tab-mode-map
;;              ("t r" . perject-tab-recent)
;;              ("t p" . perject-tab-previous)
;;              ("t n" . perject-tab-next)
;;              ("t s" . perject-tab-set)
;;              ("t c" . perject-tab-cycle-state)
;;              ("t c" . perject-tab-create)
;;              ("t d" . perject-tab-delete)
;;              ("t x" . perject-tab-reset)
;;              ("t i" . perject-tab-increment-index)
;;              ("t I" . perject-tab-decrement-index)))

(use-package perject-consult
  :after (perject consult)

  :config
  ;; Hide the list of all buffers by default and set narrowing to all buffers to space.
  (consult-customize consult--source-buffer :hidden t :narrow 32)
  (consult-customize consult--source-hidden-buffer :narrow ?h)
  (add-to-list 'consult-buffer-sources 'perject-consult--source-collection-buffer)
  (add-to-list 'consult-buffer-sources 'perject-consult--source-project-buffer))


;;;; Footer

(provide 'yyuu-mod-emacs-uix-space)

;;; yyuu-mod-emacs-uix-space.el ends here
