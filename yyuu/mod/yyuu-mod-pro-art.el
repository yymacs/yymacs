;;; yyuu-mod-pro-art.el ---                          -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Yuu Yin

;; Author: Yuu Yin <yuu@anche.no>
;; Keywords: extensions

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(use-package edraw-org
  :after (org)
  :demand t

  :config
  (edraw-org-setup-default)

  :bind
  (:map org-mode-map
        ("C-c c d s" . edraw-org-link-export-svg-at-point)))


;;;; Diagram, modeling


;;;;; DOT, graphviz

(use-package graphviz-dot-mode
  :custom
  (graphviz-dot-indent-width 4)
  (graphviz-dot-preview-extension "svg")
  (graphviz-dot-auto-preview-on-save t)

  :hook
  (graphviz-dot-mode . company-mode))


;;;;;; Org, Babel

;; https://orgmode.org/worg//org-contrib/babel/languages/ob-doc-dot.html#org95b9f0a
(use-package org
  :config
  ;; graphviz-dot-mode adds by default
  ;; (add-to-list org-src-lang-modes '("dot" . graphviz-dot))

  (org-babel-do-load-languages
   'org-babel-load-languages
   '((dot . t))))


;;;;; PlantUML

(use-package plantuml-mode
  :custom
  (plantuml-executable-path (executable-find "plantuml"))
  (plantuml-java-command (executable-find "java"))
  (plantuml-jar-path (executable-find "plantuml"))
  (plantuml-default-exec-mode
   (cond ((executable-find "plantuml") 'executable)
         ((file-exists-p plantuml-jar-path) 'jar)
         (plantuml-default-exec-mode)))
  (plantuml-output-type "svg")
  (org-plantuml-jar-path plantuml-jar-path)
  (org-plantuml-executable-path )
  (org-plantuml-exec-mode plantuml-default-exec-mode)
  (org-babel-plantuml-svg-text-to-path t))

;;;;;; Org, Babel

(use-package ob-plantuml
  :after
  (org)

  :custom
  (org-babel-plantuml-svg-text-to-path t)

  :config
  (add-to-list 'org-babel-default-header-args:plantuml
               '(:cmdline . "-charset utf-8")))

(use-package org-src
  :config
  (add-to-list
   'org-src-lang-modes '("plantuml" . plantuml)))


;;;; Footer

(provide 'yyuu-mod-pro-art)

;;; yyuu-mod-pro-art.el ends here
