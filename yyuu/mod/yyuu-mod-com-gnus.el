;;; yyuu-mod-com-gnus.el --- yyuu Gnus                -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Yuu Yin

;; Author: Yuu Yin <yuu@anche.no>
;; Keywords: extensions

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(use-package gnus
  :custom
  (gnus-home-directory (expand-file-name "gnus/" yy-state-directory))
  (gnus-directory (expand-file-name "news/" gnus-home-directory))
  ;; Unix news readers use a shared startup file: `newsrc'. It contains
  ;; information about what groups are subscribed, and which articles in these
  ;; groups have been read.
  (gnus-startup-file (expand-file-name "newsrc" gnus-home-directory))
  ;; Dribble is a special buffer which Gnus auto adds data which user changes,
  ;; so the user can recover from it if not saved before e.g. GNU Emacs failure.
  (gnus-dribble-directory (expand-file-name "dribble/" gnus-home-directory))
  ;; `newsrc' is not useful when the user does not use something else than Gnus.
  (gnus-save-newsrc-file nil)
  ;; No need for `newsrc' as the user only uses Gnus for such functionality,
  ;; which can use `newsrc.eld' instead (`.eld' means El Dingo).
  (gnus-read-newsrc-file nil)
  (gnus-cache-directory (expand-file-name "gnus/" yy-cache-directory))
  (gnus-article-save-directory gnus-directory)
  (gnus-kill-files-directory gnus-directory)

  ;; (gnus-select-method '(nntp "news"))
  (gnus-select-method '(nnnil ""))

  :config
  ;; Add foreign/remote news groups.
  (add-to-list 'gnus-secondary-select-methods '(nntp "news.gnus.org"))

  :bind
  ("C-c k g" . #'gnus))

(use-package gnus
  :after
  nnrss

  :custom
  (nnrss-group-alist nil))


;;;; Footer

(provide 'yyuu-mod-com-gnus)

;;; yyuu-mod-com-gnus.el ends here
