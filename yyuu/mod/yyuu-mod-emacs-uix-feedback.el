;;; yyuu-mod-emacs-uix-feedback.el --- yyuu's user interface feedback  -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Yuu Yin

;; Author: Yuu Yin <yuu@anche.no>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(use-package which-key
  :hook
  (after-init . which-key-mode)
  :custom
  (which-key-idle-delay 0.4)
  (which-key-allow-imprecise-window-fit nil)
  (which-key-sort-order #'which-key-key-order-alpha)
  (which-key-sort-uppercase-first nil))

(use-package keycast
  :bind
  ("C-c t k" . keycast-mode))

(provide 'yyuu-mod-emacs-uix-feedback)
;;; yyuu-mod-emacs-uix-feedback.el ends here
