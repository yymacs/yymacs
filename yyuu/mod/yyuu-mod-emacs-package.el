;;; yyuu-mod-emacs-package.el --- yyuu's package               -*- lexical-binding: t; -*-

;; Copyright (C) 2022--2023  Yuu Yin

;; Author: Yuu Yin <yuu@anche.no>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:


;;;; Requirements

(require 'package)
(require 'package-vc)
(eval-when-compile
  (require 'use-package))


;;;; Definitions

(defmacro yyuu-package-install (package &optional feature &rest body)
  "Set up PACKAGE from an Elisp archive, with rest BODY; require
FEATURE instead of PACKAGE if given.

PACKAGE is a quoted symbol, FEATURE is a quoted symbol, while BODY
consists of balanced expressions.

Try to install the package if it is missing.

Reference: https://git.sr.ht/~protesilaos/dotfiles/tree/master/item/emacs/.emacs.d/init.el#L101"
  (declare (indent 1))
  `(progn
     ;; replace when not with unless?
     (unless (package-installed-p ,package)
       (unless package-archive-contents
         (package-refresh-contents))
       (package-install ,package))
     ;; (if (require ,package nil 'noehvrror)
     ;;     (progn ,@body)
     ;;   (display-warning 'yy
     ;;                    (format "Loading `%s' failed" ,package)
     ;;                    :warning))
     ))

(defmacro yyuu-package-vc-install (package &optional rev backend name body)
  "If PACKAGE is a cons-cell, it should have the form (NAME . SPEC),
where NAME is a symbol indicating the package name and SPEC is a
plist as described in `package-vc-selected-packages'. See also
`package-vc-install'."
  (declare (indent 1))
  `(progn
     (when (consp ,package)
       (unless (package-installed-p (car ,package))
         (unless package-archive-contents
           (package-refresh-contents))
         (package-vc-install ,package)))))


;;;; Package

;; Set archives.
(dolist (package-archive '(("melpa" . "https://melpa.org/packages/"))
                         package-archives)
  (unless (assoc-default (car package-archive) package-archives)
    (add-to-list 'package-archives package-archive t)))

;; Initialize.
(package-initialize)


;;;; use-package

(use-package package
  :bind
  ("C-c e p d" . package-delete)
  ("C-c e p i" . package-install-selected-packages)
  ("C-c e p r" . package-refresh-contents)
  ("C-c e p R" . package-quickstart-refresh)
  ("C-c e p u" . package-update)
  ("C-c e p U" . package-update-all))

(use-package package-vc
  :bind
  ("C-c e p v i" . package-vc-install-selected-packages)
  ("C-c e p v u" . package-vc-update)
  ("C-c e p v U" . package-vc-update-all))


;;;; Footer

(provide 'yyuu-mod-emacs-package)

;;; yyuu-mod-emacs-package.el ends here
