;;; yyuu-mod-comp-lang-lisp.el --- yyuu's LISP                     -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Yuu Yin

;; Author: Yuu Yin <yuu@anche.no>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;


;;; Code:


;;;; Scheme


;;;;; GNU Guile

(use-package geiser-guile
  :defer t

  :config
  (add-to-list 'geiser-guile-load-path "/mnt/pro/dev/guix/master")
  (add-to-list 'geiser-guile-load-path "/mnt/etc/guixs")
  (add-to-list 'geiser-guile-load-path "/ssh:root@192.168.122.59#22:/mnt/etc/guixs")
  (add-to-list 'geiser-guile-load-path "/mnt/etc/guixs/yuuguix")
  (add-to-list 'geiser-guile-load-path "/ssh:root@192.168.122.59#22:/mnt/etc/guixs/yuuguix")

  :hook
  (geiser-debug-mode . visual-line-mode))


(provide 'yyuu-mod-comp-lang-lisp)
;;; yyuu-mod-comp-lang-lisp.el ends here
