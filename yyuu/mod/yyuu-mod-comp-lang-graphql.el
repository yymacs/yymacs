;;; yyuu-mod-comp-lang-graphql.el --- GraphQL        -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Yuu Yin

;; Author: Yuu Yin <yuu@anche.no>
;; Keywords: extensions

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(use-package graphql-mode
  :defer t)

(use-package ob-graphql
  :after (org)
  :defer t)

;; https://emacs-lsp.github.io/lsp-mode/page/lsp-graphql/
;; https://github.com/graphql/graphiql/tree/main/packages/graphql-language-service-cli#readme


(provide 'yyuu-mod-comp-lang-graphql)
;;; yyuu-mod-comp-lang-graphql.el ends here
