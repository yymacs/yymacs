;;; yyuu-mod-emacs-uix-search.el --- yyuu's user interface search  -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Yuu Yin

;; Author: Yuu Yin <yuu@anche.no>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:


;;;; Buffer

(use-package isearch
  :custom
  (isearch-lazy-count t)
  (isearch-lazy-highlight t))

;; (use-package isearchb
;;   :custom
;;   (isearch-lazy-count t)
;;   (isearch-lazy-highlight t))

;; The only search compatible with multiple-cursors.
(use-package phi-search)


;;;; File

(use-package recentf
  :ensure t

  :custom
  (recentf-max-saved-items 100)

  :hook
  (after-init . recentf-mode)

  ;; :bind
  ;; ("C-c f f" . recentf-open)
  ;; ("C-c f r o" . recentf-open)
  ;; ("C-c f F" . recentf-open-files)
  ;; ("C-c f r O" . recentf-open-files)
  ;; ("C-c f r f" . recentf-open)
  ;; ("C-c f r r" . recentf-open)
  ;; ("C-c f r s" . recentf-save-list)
  ;; ("C-c f r e" . recentf-edit-list)
  )


(provide 'yyuu-mod-emacs-uix-search)
;;; yyuu-mod-emacs-uix-search.el ends here
