;;; yyuu-mod-pro-ai.el --- Artificial intelligence tooling, assistants, ...  -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Yuu Yin

;; Author: Yuu Yin <yuu@anche.no>
;; Keywords: extensions

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:


;;;; chatbotter

(use-package gptel
  :defer t

  :custom
  (gptel-model "gemini-pro")
  ;; (gptel-model "gpt-3.5-turbo")
  ;; (gptel-model "gpt-4")

  (gptel-default-mode #'org-mode)

  :bind
  ("C-c i i" . gptel)
  ("C-c i m" . gptel-mode)
  ("C-c i s" . gptel-send)
  ("C-c i e" . gptel-menu))

(use-package gptel-curl)

(use-package org-ai
  :ensure t
  :commands (org-ai-mode
             org-ai-global-mode)

  :custom
  (org-ai-image-directory (expand-file-name "asset/image/org-ai" org-directory))
  ;; (org-ai-default-chat-model . "gpt-4")

  :hook
  (org-mode . org-ai-mode)

  :init
  ;; Install global keybindings on C-c M-a
  (org-ai-global-mode))


;;;; Software development

(use-package starhugger
  :bind
  ("C-c d i i" . starhugger-trigger-suggestion)
  ("C-c d i n" . starhugger-next-suggestion)
  ("C-c d i p" . starhugger-prev-suggestion)
  ("C-c d i a" . starhugger-accept-suggestion)
  ("C-c d i I" . starhugger-accept-suggestion)
  ("C-c d i q" . starhugger-querry))

(use-package codeium)


;;;; Footer

(provide 'yyuu-mod-pro-ai)

;;; yyuu-mod-pro-ai.el ends here
