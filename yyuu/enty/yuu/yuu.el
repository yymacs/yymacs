;;; yuu.el --- yyuu's my -*- lexical-binding: t; -*-

;; Copyright (C) 2022--2023  Yuu Yin <yuu@anche.no>
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;; Author: Yuu Yin <yuu@anche.no>
;; Maintainer: Yuu Yin <yuu@anche.no>
;; Homepage: https://codeberg.org/yymacs/yyuu

;; Package-Requires:


;;; Commentary:


;;; Code:


;;;; Libraries

(require 'yyuu-mod-lib)
(require 'yyuu-mod-emacs-package)


;;;; Packages

(require 'yyuu-mod-packages)


;;;; Module

(require 'yyuu-mod-com-gnus)
(require 'yyuu-mod-comp-conf-manage)
(require 'yyuu-mod-comp-lang)
(require 'yyuu-mod-comp-lang-c)
(require 'yyuu-mod-comp-lang-elixir)
(require 'yyuu-mod-comp-lang-go)
(require 'yyuu-mod-comp-lang-graphql)
(require 'yyuu-mod-comp-lang-haskell)
(require 'yyuu-mod-comp-lang-java)
(require 'yyuu-mod-comp-lang-javascript)
(require 'yyuu-mod-comp-lang-julia)
;; (require 'yyuu-mod-comp-lang-lean)
(require 'yyuu-mod-comp-lang-lisp)
(require 'yyuu-mod-comp-lang-misc)
(require 'yyuu-mod-comp-lang-nix)
(require 'yyuu-mod-comp-lang-python)
(require 'yyuu-mod-comp-lang-r)
(require 'yyuu-mod-comp-lang-rust)
(require 'yyuu-mod-comp-system)
(require 'yyuu-mod-comp-web)
(require 'yyuu-mod-emacs-fix)
(require 'yyuu-mod-emacs-uix)
(require 'yyuu-mod-emacs-uix-bar)
(require 'yyuu-mod-emacs-uix-buffer)
(require 'yyuu-mod-emacs-uix-column)
(require 'yyuu-mod-emacs-uix-completion)
(require 'yyuu-mod-emacs-uix-edit)
(require 'yyuu-mod-emacs-uix-face)
(require 'yyuu-mod-emacs-uix-feedback)
(require 'yyuu-mod-emacs-uix-input)
(require 'yyuu-mod-emacs-uix-line)
(require 'yyuu-mod-emacs-uix-modeline)
(require 'yyuu-mod-emacs-uix-search)
(require 'yyuu-mod-emacs-uix-space)
(require 'yyuu-mod-emacs-uix-text)
(require 'yyuu-mod-emacs-uix-theme)
(require 'yyuu-mod-emacs-uix-window)
(require 'yyuu-mod-pro-art)
(require 'yyuu-mod-pro-doc)
(require 'yyuu-mod-pro-lang)
(require 'yyuu-mod-pro-math)
(require 'yyuu-mod-pro-misc)
(require 'yyuu-mod-pro-org)
(require 'yyuu-mod-pro-ai)
(require 'yyuu-mod-pro-tex)

(require 'yyuu-mod-emacs-uix-input-key)


;;;; Customization

(require 'yuu-custom)

;; Set the custom file directory to here.
(require 'cus-edit)
(setq-default custom-file
              (expand-file-name "yuu-custom-custom.el" yuu-directory))
(load custom-file)


;;;; Footer

(provide 'yuu)

;;; yuu.el ends here
