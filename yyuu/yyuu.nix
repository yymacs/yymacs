{
  ab = [
    # EMACS
    solarized-theme
    embark
    marginalia
    multiple-cursors
    perject
    rainbow-mode
    sudo-edit
    tabspaces
    vertico
    which-key
    fussy
    vc-use-package
    vterm
    winum

    # COMP
    geiser-guile
    haskell-mode
    magit
    markdown-mode
    nix-mode
    restclient
    rustic
    smartparens

    # PRO
    calibredb
    citar-embark
    citar-org-roamx
    citeproc
    elfeed
    nov
    org-pomodoro
    org-roam
    org-roam-bibtex
    pdf-tools
    spell-fu

    # _
    emacsql-sqlite # for org-roam
    emojify
    emojify-logos
  ];
}
