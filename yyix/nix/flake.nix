## flake.nix

## COMMENTARY

{
  description = "yymacs: YY Emacs";

  nixConfig = {
    extra-substituters = [
      "https://nix-community.cachix.org"
    ];

    extra-trusted-public-keys = [
      "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs="
    ];
  };

  inputs = {
    ### NIXPKGS

    nixpkgs = {
      type = "github";
      owner = "nixos";
      repo = "nixpkgs";
      rev = "677ed08a50931e38382dbef01cba08a8f7eac8f6";
    };

    ### EMACS

    # For changing the commit hash, see:
    # https://github.com/nix-community/emacs-overlay/issues/122#issuecomment-1016778377
    # https://github.com/nix-community/emacs-overlay/issues/122#issuecomment-1193081483
    # https://hydra.nix-community.org/job/emacs-overlay/unstable/emacsGit
    # NOTE: emacs-overlay and nixpkgs revisions
    # need to be the same as those used by Hydra
    # as to hit nix-community substituter/cache.
    emacs-overlay = {
      type = "github";
      owner = "nix-community";
      repo = "emacs-overlay";
      # https://hydra.nix-community.org/build/1378010#tabs-buildinputs
      rev = "42fdbe1322a4d9bdafa68b9ec94f477994d7b919";
      # no flake.lock, but doesn't work anyway for hitting cache.
      inputs.nixpkgs = {
        type = "github";
        owner = "nixos";
        repo = "nixpkgs";
        rev = "befc83905c965adfd33e5cae49acb0351f6e0404";
      };
    };
  };

  outputs = (inputs@{
    self
    , nixpkgs
    , emacs-overlay
    , ...
  }:
    let
      inherit (nixpkgs.lib) filterAttrs;
      inherit (builtins) mapAttrs elem;
      inherit (self) outputs;

      lib = import nixpkgs.lib;

      supportedSystems = [
        "x86_64-linux"
        "aarch64-linux"
      ];

      # Partial application.
      forAllSystems = nixpkgs.lib.genAttrs supportedSystems;

      devShells = forAllSystems (system: {
        default = nixpkgs.legacyPackages.${system}.callPackage ./shell.nix {
          pkgs = nixpkgs.legacyPackages.${system};
        };
      });
  );
}
