
;;; Code:



;;;; MODULE
(define-module (yy channels)
  #:use-module (guix channels)		; for channel
  #:export (yy-channels))


;;; CHANNEL
(define-public (yy-channels)
  (list (channel
	 (name 'guix)
	 (url "https://git.savannah.gnu.org/git/guix.git")
	 (introduction
          (make-channel-introduction
           "cbe8391d7c570a12416acfbfbbb818404d8b9203"
           (openpgp-fingerprint
            "BBB0 2DDF 2CEA F6A8 0D1D  E643 A2A0 6DF2 A33A 54FA"))))
	(channel
	 (name 'nonguix)
	 (url "https://gitlab.com/nonguix/nonguix.git")
         (introduction
          (make-channel-introduction
           "2182f808b36e9c76ebf014ab4888d59a66686f26"
           (openpgp-fingerprint
            "2A39 3FFF 68F4 EF7A 3D29  12AF 6F51 20A0 22FB B2D5"))))))



;;; channels.scm ends here
