;;; manifest.scm --- yymacs' Guix definition -*- lexical-binding: t; -*-

;; Copyright (C) 2022--2023  Yuu Yin <yuu@anche.no>
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;; Author: Yuu Yin <yuu@anche.no>
;; Maintainer: Yuu Yin <yuu@anche.no>
;; Created: 2022-07-07
;; Modified: 2022-07-07
;; Version: 2022.10-main
;; Keywords:
;; Homepage: https://codeberg.org/yymacs/yymacs

;; This file is part of yymacs.


;;; Commentary:
;; This "manifest" file can be passed to 'guix package --manifest=FILE' to
;; reproduce the content of your profile. This is "symbolic": it only specifies
;; package names. To reproduce the exact same profile, you also need to capture
;; the channels being used, as returned by "guix describe". See the "Replicating
;; Guix" section in the manual.


;;; Code:


;;;; Load
(let ((yy-path-root (dirname (current-filename))))
  (add-to-load-path yy-path-root)
  (add-to-load-path (string-append yy-path-root "/" "yy"))
  %load-path)

;; Load yy defined channels file.
(load-from-path "channels")


;;;; Module

(define-module (yy)
  #:use-module (guix profiles)		; packages->manifest
  #:use-module (gnu packages base)	; glibc-locales
  #:use-module (gnu packages emacs)     ; emacs-next
  #:use-module (yy channels))		; yy-channels


;;;; Channel
(yy-channels)


;;;; Manifest

(packages->manifest
 (list
  ;; Guix
  glibc-locales

  ;; Emacs
  emacs-next

  ;; Misc
  git
  gnupg
  openssh
  gnutls))


;;; manifest.scm ends here
