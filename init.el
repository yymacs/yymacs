;;; init.el --- yymacs's init -*- lexical-binding: t; -*-

;; Copyright (C) 2022--2023  Yuu Yin <yuu@anche.no>
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;; Author: Yuu Yin <yuu@anche.no>
;; Maintainer: Yuu Yin <yuu@anche.no>
;; Homepage: https://codeberg.org/yymacs/yymacs

;; Package-Requires:


;;; Commentary:


;;; Code:


;;;; Load

(condition-case err
    ;; Load yyuu[r] init.
    (load yyuu-file nil (not init-file-debug) nil 'must-suffix)
  (file-missing
   nil))


;;; init.el ends here
