;;; yy.el --- yuumacs' core -*- lexical-binding: t; -*-

;; Copyright (C) 2022--2023  Yuu Yin <yuu@anche.no>
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;; Author: Yuu Yin <yuu@anche.no>
;; Maintainer: Yuu Yin <yuu@anche.no>
;; Created: 2022-07-07
;; Modified: 2022-07-07
;; Version: 2022.09-main
;; Keywords:
;; Homepage: https://codeberg.org/yymacs/yymacs
;; Package-Requires: ((emacs "29"))

;; This file is not part of GNU Emacs.

;;; Commentary:


;;; Code:


;;;; Check early

;; Version check.
(eval-and-compile
  (let ((yy-emacs-minimum-supported-version 29))
    (when (< emacs-major-version yy-emacs-minimum-supported-version)
      (user-error
       (format "Your Emacs version is %s, but minimum supported version is %s"
               emacs-version
               yy-emacs-minimum-supported-version)))))



;;;; Load

(setq-default load-prefer-newer t)


;;;; Require built-ins

;; For cl-lib, at least.
(eval-when-compile (require 'subr-x))


;;;;; Require yys

(add-to-list 'load-path (file-name-directory load-file-name))

;; Load yy's standard library.
(require 'yy-lib)

;; Load yy's global definitions.
(require 'yy-global)

;; Load yy's default settings.
(require 'yy-default)

;; Load yy's hooks.
(require 'yy-hook)

;; Load yy's optimizations.
(require 'yy-optimization-performance)
(require 'yy-optimization-security)


(provide 'yy)
;;; yy.el ends here
