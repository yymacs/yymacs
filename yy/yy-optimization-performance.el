;;; yy-optimization-performance.el --- yymacs's perfomance optimizations -*- lexical-binding: t; -*-

;; Copyright (C) 2022--2023  Yuu Yin <yuu@anche.no>
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;; Author: Yuu Yin <yuu@anche.no>
;; Maintainer: Yuu Yin <yuu@anche.no>
;; Homepage: https://codeberg.org/yymacs/yymacs
;; Package-Requires: ((emacs "29.0"))

;;; Commentary:


;;; Code:


;;;; Load

;; (require 'yy-hook)
(require 'yy-global)


;;;; Garbage Collection

;; Set `gc-cons-threshold' because most Emacs users and configs can benefit from
;; it. Setting it to `most-positive-fixnum' is dangerous if downstream does not
;; reset it later to something reasonable, so use 224mb as a best fit guess. It
;; should be better than Emacs' 80kb default.
(setq gc-cons-threshold (* 224 1024 1024))

;; Ref.: https://matrix.to/#/!WfZsmtnxbxTdoYPkaT:greyface.org/$9mp8Bi_weiKiOZvZS_46z6gXuOz9tE-Aroi-hJKlIAw?via=matrix.org&via=envs.net&via=kde.org
(setq gc-cons-percentage 0.0001)

;;;; Native compilation

;; Native compilation support (see http://akrl.sdf.org/gccemacs.html)
(when (boundp 'native-comp-eln-load-path)

  ;; Store eln files in proper cache directory instead EMACSDIR/eln-cache.
  ;; (this is a workaround on what is set on emacs/lisp/startup.el).
  (startup-redirect-eln-cache (expand-file-name "eln/" yy-cache-directory))

  ;; UX: By default, native-comp uses 100% of half your cores. If you are
  ;;   expecting this, it should be no issue, but the sudden (and silent) spike
  ;;   of CPU and memory utilization can alarm folks, overheat laptops, or
  ;;   overwhelm less performant systems.
  (define-advice comp-effective-async-max-jobs (:before (&rest _)
                                                        set-default-cpus)
    "Default to 1/4 of cores in interactive sessions and all of them otherwise."
    (and (null comp-num-cpus)
         (zerop native-comp-async-jobs-number)
         (setq comp-num-cpus
               (max 1 (/ (num-processors) (if noninteractive 1 4)))))))


(provide 'yy-optimization-performance)
;;; yy-optimization-performance.el ends here
