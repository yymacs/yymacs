;;; yy-benchmark.el --- yymacs's benchmarking -*- lexical-binding: t; -*-

;; Copyright (C) 2022--2023  Yuu Yin <yuu@anche.no>
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;; Author: Yuu Yin <yuu@anche.no>
;; Maintainer: Yuu Yin <yuu@anche.no>
;; Homepage: https://codeberg.org/yymacs/yymacs

;;; Commentary:


;;; Code:

(defvar yy-init-time nil
  "The time it took, in seconds, for yy to initialize.")

(defun yy-display-benchmark-h (&optional return-p)
  "Display a benchmark including number of packages and modules loaded.

If RETURN-P, return the message as a string instead of displaying it."
  (funcall (if return-p #'format #'message)
           "Yy loaded %d packages across %d modules in %.03fs"
           (- (length load-path) (length (get 'load-path 'initial-value)))
           (hash-table-count yy-modules)
           (or yy-init-time
               (setq yy-init-time
                     (float-time
                      (time-subtract (current-time) before-init-time))))))

(add-hook 'yy-after-init-hook #'yy-display-benchmark-h 110)


(provide 'yy-benchmark)
;;; yy-benchmark.el ends here
