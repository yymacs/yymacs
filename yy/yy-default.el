;;; yy-default.el --- yymacs's defaults -*- lexical-binding: t; -*-

;; Copyright (C) 2022--2023  Yuu Yin <yuu@anche.no>
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;; Author: Yuu Yin <yuu@anche.no>
;; Maintainer: Yuu Yin <yuu@anche.no>
;; Homepage: https://codeberg.org/yymacs/yymacs

;;; Commentary:


;;; Code:

;; (require 'yy-global)


;;;; Tide Emacs directory

;; Do NOT litter `yy-emacs-directory'/`HOME'.

;; HACK: Change `user-emacs-directory' to a stateful directory because many
;;   packages (even built-in ones) abuse it to build paths for storage/cache
;;   files (instead of correctly using `locate-user-emacs-file'). This change
;;   ensures that said data files are never saved to the root of your emacs
;;   directory AND saves us the trouble of setting a million directory/file
;;   variables. BUT it may throw off users, packages that uses it to search for
;;   users' inits.
(setq user-emacs-directory yy-state-directory)

;; ...BUT, this also may surprise users and packages that read
;; `user-emacs-directory' expecting to find the directory of the users' Emacs
;; configuration, such as `server'!
(setq-default server-auth-dir (expand-file-name "server/" yy-cache-directory))

;; Packages/Customizations/Variables with file/directory settings that do not
;; use `user-emacs-directory' or `locate-user-emacs-file' to initialize will
;; need to set explicitly, to stop them from littering in the Emacs
;; configuration directory.
(setq-default
 package-user-dir (expand-file-name "el/" yy-state-directory)
 desktop-dirname (expand-file-name "desktop/" yy-state-directory)
 org-babel-temporary-stable-directory (expand-file-name "org-babel-tmp/" yy-cache-directory)
 pcache-directory (expand-file-name "pcache/" yy-cache-directory)
 auto-save-list-file-prefix (cond
                             ((eq system-type 'ms-dos)
                              ;; MS-DOS cannot have initial dot, and allows only 8.3 names
                              (expand-file-name "auto-save.list/_s" yy-cache-directory))
                             (t
                              (expand-file-name "auto-save-list/.saves-" yy-cache-directory))))


;;;; Custom[ize]

;; Allow the user to store custom.el-saved settings and themes in yyuu config.
(setq custom-file (expand-file-name "custom.el" yyuu-directory))

(define-advice en/disable-command (:around (fn &rest args) write-to-data-directory)
  "Save safe-local-variables to `custom-file' instead of `user-init-file'.

Otherwise, `en/disable-command' (in novice.el.gz) is hardcoded to write them to
`user-init-file')."
  (let ((user-init-file custom-file))
    (apply fn args)))


;;;; Encoding

;; Contrary to what many Emacs users have in their configs, there is no need for
;; more than the following to make UTF-8 the default coding system.
(set-language-environment "UTF-8")
;; ...but `set-language-environment' also sets `default-input-method', which is
;; a step too opinionated.
(setq default-input-method nil)
;; ...And the clipboard on Windows could be in a wider encoding (UTF-16), so
;; leave Emacs to its own devices.
(yy-eval-when (cl-case system-type ((cygwin windows-nt ms-dos)
                                    (setq selection-coding-system 'utf-8))))


;;;; File

;; Add support for additional file extensions.
(dolist (entry '(("/COPYING\\'" . text-mode)
                 ("/LICENSE\\'" . text-mode)
                 ("\\.log\\'" . text-mode)
                 ("rc\\'" . conf-mode)
                 ("\\.\\(?:hex\\|nes\\)\\'" . hexl-mode)))
  (push entry auto-mode-alist))


;;;; Package


;;;;; package.el

;; Since Emacs 27, package initialization occurs before `user-init-file' is
;; loaded, but after `early-init-file'. yy handles package initialization, so we
;; must prevent Emacs from doing it again.
;; (setq package-enable-at-startup nil)


;;;; Log

;; Reduce unnecessary/unactionable warnings/logs.

;; Disable warnings from the legacy advice API. They aren't actionable or
;; useful, and often come from third party packages.
(setq ad-redefinition-action 'accept)


;;;; Operating System


;;;;; Windows

;; Fix HOME on Windows.
;; $HOME is not normally defined on Windows, but many unix tools expect it.
(when yy-system-type-windows
  (when-let (realhome
             (and (null (getenv-internal "HOME"))
                  (getenv "USERPROFILE")))
    (setenv "HOME" realhome)
    (setq abbreviated-home-dir nil)))


(provide 'yy-default)
;;; yy-default.el ends here
