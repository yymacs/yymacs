;;; yy-lib.el --- yymacs's core standard library -*- lexical-binding: t; -*-

;; Copyright (C) 2022--2023  Yuu Yin <yuu@anche.no>
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;; Author: Yuu Yin <yuu@anche.no>
;; Maintainer: Yuu Yin <yuu@anche.no>
;; Homepage: https://codeberg.org/yymacs/yymacs



;;; Commentary:



;;; Code:



;;;; Load
(require 'yy-hook)



;;;; CUSTOM ERROR TYPES
(define-error 'yy-error "An unexpected yy error")
(define-error 'yy-hook-error "Error in a yy startup hook" 'yy-error)



;;;; HELPERS
;; (defun yy-run-hooks (&rest hooks)
;;   "Run HOOKS (a list of hook variable symbols) with better error handling.
;; Is used as advice to replace `run-hooks'."
;;   (dolist (hook hooks)
;;     (condition-case-unless-debug e
;;         (run-hook-wrapped hook #'yy-run-hook)
;;       (yy-hook-error
;;        (unless debug-on-error
;;          (lwarn hook :error "Error running hook %S because: %s"
;;                 (if (symbolp (cadr e))
;;                     (symbol-name (cadr e))
;;                   (cadr e))
;;                 (caddr e)))
;;        (signal 'yy-hook-error (cons hook (cdr e)))))))

;; (defun yy-run-hook-on (hook-var trigger-hooks)
;;   "Configure HOOK-VAR to be invoked exactly once when any of the TRIGGER-HOOKS
;; are invoked *after* Emacs has initialized (to reduce false positives). Once
;; HOOK-VAR is triggered, it is reset to nil.

;; HOOK-VAR is a quoted hook.
;; TRIGGER-HOOK is a list of quoted hooks and/or sharp-quoted functions."
;;   (dolist (hook trigger-hooks)
;;     (let ((fn (make-symbol (format "chain-%s-to-%s-h" hook-var hook))))
;;       (fset
;;        fn (lambda (&rest _)
;;             ;; Only trigger this after Emacs has initialized.
;;             (when (and after-init-time
;;                        (or (daemonp)
;;                            ;; In some cases, hooks may be lexically unset to
;;                            ;; inhibit them during expensive batch operations on
;;                            ;; buffers (such as when processing buffers
;;                            ;; internally). In these cases we should assume this
;;                            ;; hook wasn't invoked interactively.
;;                            (and (boundp hook)
;;                                 (symbol-value hook))))
;;               (yy-run-hooks hook-var)
;;               (set hook-var nil))))
;;       (cond ((daemonp)
;;              ;; In a daemon session we don't need all these lazy loading
;;              ;; shenanigans. Just load everything immediately.
;;              (add-hook 'after-init-hook fn 'append))
;;             ((eq hook 'find-file-hook)
;;              ;; Advise `after-find-file' instead of using `find-file-hook'
;;              ;; because the latter is triggered too late (after the file has
;;              ;; opened and modes are all set up).
;;              (advice-add 'after-find-file :before fn '((depth . -101))))
;;             ((add-hook hook fn -101)))
;;       fn)))



;;;; EVAL
;; (defmacro yy-eval-if (cond then &rest body)
;;   "Expands to THEN if COND is non-nil, to BODY otherwise.
;; COND is checked at compile/expansion time, allowing BODY to be omitted entirely
;; when the elisp is byte-compiled. Use this for forms that contain expensive
;; macros that could safely be removed at compile time."
;;   (declare (indent 2))
;;   (if (eval cond)
;;       then
;;     (macroexp-progn body)))

(defmacro yy-eval-when (cond &rest body)
  "Expands to BODY if CONDITION is non-nil at compile/expansion time.
See `yy-eval-if' for details on this macro's purpose."
  (declare (indent 1))
  (when (eval cond)
    (macroexp-progn body)))



(provide 'yy-lib)
;;; yy-lib.el ends here
