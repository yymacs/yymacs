;;; yy-hook.el --- yymacs's hooks -*- lexical-binding: t; -*-

;; Copyright (C) 2022--2023  Yuu Yin <yuu@anche.no>
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;; Author: Yuu Yin <yuu@anche.no>
;; Maintainer: Yuu Yin <yuu@anche.no>
;; Homepage: https://codeberg.org/yymacs/yymacs

;;; Commentary:


;;; Code:


;;;; Definitions

(defcustom yy-before-init-hook ()
  "A hook run after yy's core has initialized; before user configuration.

This is triggered right before $YYDIR/init.el is loaded, in the context of
early-init.el. Use this for configuration at the latest opportunity before the
session becomes unpredictably complicated by user config, packages, etc. This
runs in both interactive and non-interactive contexts, so guard hooks
appropriately against `noninteractive' or the `cli' context (see
`yy-context').

In contrast, `before-init-hook' is run just after $YYDIR/init.el is loaded,
but long before your modules and $YYDIR/config.el are loaded."
  :group 'yy
  :type 'hook)

(defcustom yy-after-init-hook ()
  "A hook run once yy's core and modules, and the user's config are loaded.

This triggers at the absolutel atest point in the eager startup process, and
runs in both interactive and non-interactive sessions, so guard hooks
appropriately against `noninteractive' or the `cli' context."
  :group 'yy
  :type 'hook)

(defcustom yy-first-input-hook ()
  "Transient hooks run before the first user input."
  :type 'hook
  :local 'permenant-local
  :group 'yy)

(defcustom yy-first-file-hook ()
  "Transient hooks run before the first interactively opened file."
  :type 'hook
  :local 'permenant-local
  :group 'yy)

(defcustom yy-first-buffer-hook ()
  "Transient hooks run before the first interactively opened buffer."
  :type 'hook
  :local 'permenant-local
  :group 'yy)


(provide 'yy-hook)
;;; yy-hook.el ends here
