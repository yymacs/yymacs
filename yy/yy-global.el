;;; yy-global.el --- yymacs's globals -*- lexical-binding: t; -*-

;; Copyright (C) 2022--2023  Yuu Yin <yuu@anche.no>
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;; Author: Yuu Yin <yuu@anche.no>
;; Maintainer: Yuu Yin <yuu@anche.no>
;; Homepage: https://codeberg.org/yymacs/yymacs


;;; Commentary:


;;; Code:

(require 'cl-seq)
(require 'xdg)


;;;; Group

(defgroup yy nil
  "Base GNU Emacs configuration for yλ universe"
  :link '(url-link "https://codeberg.org/yymacs/yymacs")
  :group 'environment)


;;;; Configuration management

(defconst yy-version "2023.02-main"
  "Current version of yy.")


;;;; System

(defconst yy-system-type-windows
  (cl-member system-type '(cygwin ms-dos windows-nt)))

(defconst yy-system-type-gnu-or-unix
  (cl-member system-type '(gnu gnu/linux gnu/kfreebsd berkeley-unix)))

(defconst yy-system-type-darwin
  (cl-member system-type '(darwin)))


;;;; Directory


;;;;; Source code

(defconst yy-emacs-directory user-emacs-directory
  "Path to the currently loaded Emacs directory. Must end with a slash.")

(defconst yy-directory
  (expand-file-name "yy/" yy-emacs-directory)
  "yy's core files' root directory. Must end with a slash.")

(defconst yyix-directory
  (expand-file-name "yyix/" yy-emacs-directory)
  "yy's core files' root directory. Must end with a slash.")

(defcustom yyuu-directory
  (expand-file-name "yyuu/" yy-emacs-directory)
  "yyuu[r] root directory. Must end with a slash."
  :group 'yy
  :type '(directory))

(defconst yyuu-file
  (expand-file-name "yyuu.el" yyuu-directory)
  "Users' (i.e. yyuu) main configuration file.")


;;;;; State

(defcustom yy-cache-directory
  (cl-case system-type
    ((gnu/linux gnu/kfreebsd berkeley-unix)
     (expand-file-name
      "yy/"
      ;; (or (getenv-internal "XDG_CACHE_HOME")
      ;;     (expand-file-name ".cache" (getenv-internal "HOME"))
      ;;     (expand-file-name ".cache" "~/"))
      (xdg-cache-home)))
    (darwin (expand-file-name
             "Caches"
             (or (getenv-internal "HOME")
                 "~/")))
    ((cygwin windows-nt ms-dos) (expand-file-name
                                 "yy/cache"
                                 (getenv-internal "APPDATA"))))
  "Where yy stores its global cache files.

Cache files represent non-essential data that should not be
problematic when deleted (besides, perhaps, a one-time
performance hit), lack portability (and so shouldn't be copied to
other systems/configs), and are regenerated when needed, without
user input.

Examples: images/data caches, elisp bytecode, natively compiled elisp,
session files, ELPA archives, authinfo files, org-persist."
  :group 'yy
  :type '(directory))

(defcustom yy-state-directory
  (cl-case system-type
    ((gnu/linux gnu/kfreebsd berkeley-unix)
     (expand-file-name
      "yy/"
      ;; (or (getenv-internal "XDG_STATE_HOME")
      ;;     (expand-file-name ".local/state" (getenv-internal "HOME"))
      ;;     (expand-file-name ".local/state" "~/"))
      (xdg-state-home)))
    (darwin
     (expand-file-name
      "yy/"
      (or (expand-file-name "Application Support/"
                            (getenv-internal "HOME"))
          "~/Application Support/")))
    ((cygwin windows-nt ms-dos)
     (expand-file-name
      "yy/state"
      (getenv-internal "APPDATA"))))
  "Where yy stores its global state files.

State files contain non-essential, unportable, but persistent
data which, if lost will not cause breakage, but may be
inconvenient as they cannot be automatically regenerated or
restored. For example, a recently-opened file list is not
essential, but losing it means losing this record, and restoring
it requires revisiting all those files.

Examples: history, logs, user-saved data, autosaves/backup files,
known projects, recent files, bookmarks."
  :group 'yy
  :type '(directory))


(provide 'yy-global)
;;; yy-global.el ends here
